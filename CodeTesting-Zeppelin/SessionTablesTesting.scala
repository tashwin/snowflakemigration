//Import all Amazon AWS Libraries -- Kinesis, Cloud Watch, & Basic Authentication
import com.amazonaws.auth.{BasicAWSCredentials, DefaultAWSCredentialsProviderChain}
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.kinesis.AmazonKinesisClient
import com.amazonaws.services.kinesis.model.PutRecordRequest
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient
import com.amazonaws.services.cloudwatch.model.{ StandardUnit, PutMetricDataRequest, MetricDatum, Dimension, StatisticSet }
//Import all the logging libraries
import org.apache.log4j.{Level, Logger}
import com.typesafe.config.{Config, ConfigFactory}
//Import required Spark Libraries
import org.apache.spark._
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext, Time}
import org.apache.spark.streaming.dstream.DStream.toPairDStreamFunctions
import org.apache.spark.streaming.kinesis.KinesisUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SQLContext, Row, DataFrame}
//Import Google IO
import com.google.common.io.Files
//Import Java Libraries
import java.net.URI
import java.util.concurrent.TimeUnit
import java.util.{Date, TimeZone, Calendar}
//Import Scala Libraries
import scala.collection.JavaConversions._
import scala.reflect.ClassTag
//Import Json4s libraries
import org.json4s.jackson.JsonMethods
import org.json4s.jackson.JsonMethods._
import org.json4s.JsonAST._
import org.json4s.DefaultFormats
import org.json4s.JsonAST.JValue
import org.json4s.JsonAST.JNothing
import org.json4s.JsonAST.JString
import org.json4s.native.Serialization
import org.json4s.native.Serialization.{read, write}
//Import Hadoop Libraries
import org.apache.hadoop.conf.{Configurable, Configuration}
import org.apache.hadoop.util.Progress
import org.apache.hadoop.mapreduce._
import org.apache.hadoop.io.{DataInputBuffer, NullWritable, Text}
import org.apache.hadoop.fs.{FileSystem,Path}
import org.apache.hadoop.mapreduce.lib.output.{TextOutputFormat, LazyOutputFormat, MultipleOutputs}
import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat
import org.apache.hadoop.mapreduce.task.{ReduceContextImpl, TaskAttemptContextImpl}
import org.apache.hadoop.mapred.RawKeyValueIterator
import org.apache.hadoop.mapreduce.counters.GenericCounter
import org.apache.hadoop.io.compress._
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl.DummyReporter

val myRDD = sc.textFile("s3n://ddi-business-intelligence/emr_spark_testing/JSON_MESSAGES_TESTING")
myRDD.count()


/*
--snowflake code to get metagame data (copy the messageline field only, when creating json file):

select messageline:cmd::string,
       messageline
from EVENTDBPROD.PUBLIC.EVENTLOGNRT
where messageline:cmd::string = 'initSuccess'
or    messageline:cmd::string = 'clinfo'
limit 10;
*/


myRDD.map({line => parse(line)})
            .map(json =>  {
                            implicit val formats = DefaultFormats
                            //This method returns the first match for a pattern and a string
                            def extractFirstMatchingRegex(Pattern: scala.util.matching.Regex, SearchStr: String):String ={
                                val matched = Pattern.findFirstMatchIn(SearchStr)
                                var extractedVal =""
                                matched match{
                                    case Some(m) =>
                                        extractedVal = m.group(1).toString.trim
                                    case None =>
                                        extractedVal =""
                                }
                                return extractedVal
                            }
                            val tbl_name =
                            if (compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "initSuccess"){
                                    "init_success"
                            }else if(compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "clinfo"){
                                    "cli_info"
                            }else{
                                    "other"
                            }
                            val this_records =
                            tbl_name match{

                            case "init_success" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    //Extract blue box flag
                                    val bluebox_flag =
                                        if (compact(render(json \ "b")).trim.length > 0 ){
                                            compact(render(json \ "b")).replace("\"", "").trim
                                        }else{null}
                                    //Extract psc, friend_bonus, spin_bonus, day_bonus, total_bonus, wheelstoplocation and sfs_sessionid
                                    val (psc:Double, friend_bonus:Long, spin_bonus:Long, day_bonus:Long, total_bonus:Long, wheelstoplocation:Long, sfs_sessionid:Long) =
                                        if (compact(render(json \ "params")).replace("\"", "").trim.length > 0 && compact(render(json \ "params")).replace("\"", "").trim!="null"){
                                            (
                                                if(compact(render(json \"params"\"psc")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"psc")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"psc")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble},
                                                if(compact(render(json \"params"\"numFriendsBonus")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"numFriendsBonus")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"numFriendsBonus")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"spinBonus")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"spinBonus")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"spinBonus")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"consecutiveDaysBonus")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"consecutiveDaysBonus")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"consecutiveDaysBonus")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"totalBonus")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"totalBonus")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"totalBonus")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"wheelStopLocation")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"wheelStopLocation")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"wheelStopLocation")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"sessionId")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"sessionId")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"sessionId")).replace("\"", "").trim.toLong
                                                }else{0.toLong}
                                                )
                                        }else{
                                            (0.toDouble,0.toLong,0.toLong,0.toLong,0.toLong,0.toLong,0.toLong)
                                        }
                                    //Extract cash, coins, level
                                    val (cash:Double, coins:Long, level:Long) =
                                        if (compact(render(json \ "params"\"userData")).replace("\"", "").trim.length > 0 && compact(render(json \ "params"\"userData")).replace("\"", "").trim!="null"){
                                            (
                                                if(compact(render(json\"params"\"userData"\"cash")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"userData"\"cash")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"userData"\"cash")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble},
                                                if(compact(render(json \"params"\"userData"\"coins")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"userData"\"coins")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"userData"\"coins")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"userData"\"level")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"userData"\"level")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"userData"\"level")).replace("\"", "").trim.toLong
                                                }else{0.toLong}
                                            )
                                         }else{
                                             (0.toDouble,0.toLong,0.toLong)
                                        }
                                        sessionid+","+userid+","+psc.toString+","+cash.toString+","+coins.toString+","+
                                        level.toString+","+friend_bonus.toString+","+spin_bonus.toString+","+day_bonus.toString+","+
                                        total_bonus.toString+","+wheelstoplocation.toString+","+bluebox_flag+","+sfs_sessionid.toString+","+ts.toString+","+phpSid+","+currentCID
                                }

                            case "cli_info" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract os
                                    val os =
                                        if (compact(render(json \ "os")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "os")).replace("\"", "").trim
                                        }else{null}
                                    //Extract device
                                    val device =
                                        if (compact(render(json \ "device")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "device")).replace("\"", "").trim
                                        }else{null}
                                    //Extract client type
                                    val cltype =
                                        if (compact(render(json \ "cltype")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "cltype")).replace("\"", "").trim
                                        }else{null}
                                    //Extract facebook id
                                    val fbid =
                                        if (compact(render(json \ "fbid")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "fbid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract device id
                                    val device_id =
                                        if (compact(render(json \ "device_id")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "device_id")).replace("\"", "").trim
                                        }else{null}
                                    //Extract language
                                    val lang =
                                        if (compact(render(json \ "lang")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "lang")).replace("\"", "").trim
                                        }else{null}
                                    //Extract browser
                                    val browser =
                                        if (compact(render(json \ "browser")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "browser")).trim
                                        }else{null}
                                    sessionid +","+ os  +","+ device +","+ cltype +","+ fbid +","+ device_id +","+ lang +","+ browser +","+ ts.toString
                                }

                            case default =>
                                {
                                    "not yet parsed"
                                }
                            }
                            (tbl_name,this_records)
            }).saveAsNewAPIHadoopFile("s3a://ddi-business-intelligence/emr_spark_testing/sample_21/", classOf[String], classOf[String], classOf[TextOutputFormat[String, String]])
