//Import all Amazon AWS Libraries -- Kinesis, Cloud Watch, & Basic Authentication
import com.amazonaws.auth.{BasicAWSCredentials, DefaultAWSCredentialsProviderChain}
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.kinesis.AmazonKinesisClient
import com.amazonaws.services.kinesis.model.PutRecordRequest
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient
import com.amazonaws.services.cloudwatch.model.{ StandardUnit, PutMetricDataRequest, MetricDatum, Dimension, StatisticSet }
//Import all the logging libraries
import org.apache.log4j.{Level, Logger}
import com.typesafe.config.{Config, ConfigFactory}
//Import required Spark Libraries
import org.apache.spark._
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext, Time}
import org.apache.spark.streaming.dstream.DStream.toPairDStreamFunctions
import org.apache.spark.streaming.kinesis.KinesisUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SQLContext, Row, DataFrame}
//Import Google IO
import com.google.common.io.Files
//Import Java Libraries
import java.net.URI
import java.util.concurrent.TimeUnit
import java.util.{Date, TimeZone, Calendar}
//Import Scala Libraries
import scala.collection.JavaConversions._
import scala.reflect.ClassTag
//Import Json4s libraries
import org.json4s.jackson.JsonMethods
import org.json4s.jackson.JsonMethods._
import org.json4s.JsonAST._
import org.json4s.DefaultFormats
import org.json4s.JsonAST.JValue
import org.json4s.JsonAST.JNothing
import org.json4s.JsonAST.JString
import org.json4s.native.Serialization
import org.json4s.native.Serialization.{read, write}
//Import Hadoop Libraries
import org.apache.hadoop.conf.{Configurable, Configuration}
import org.apache.hadoop.util.Progress
import org.apache.hadoop.mapreduce._
import org.apache.hadoop.io.{DataInputBuffer, NullWritable, Text}
import org.apache.hadoop.fs.{FileSystem,Path}
import org.apache.hadoop.mapreduce.lib.output.{TextOutputFormat, LazyOutputFormat, MultipleOutputs}
import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat
import org.apache.hadoop.mapreduce.task.{ReduceContextImpl, TaskAttemptContextImpl}
import org.apache.hadoop.mapred.RawKeyValueIterator
import org.apache.hadoop.mapreduce.counters.GenericCounter
import org.apache.hadoop.io.compress._
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl.DummyReporter

val myRDD = sc.textFile("s3n://ddi-business-intelligence/emr_spark_testing/JSON_MESSAGES_TESTING")
myRDD.count()

myRDD.map({line => parse(line)})
            .map(json =>  {
                            implicit val formats = DefaultFormats
                            //This method returns the first match for a pattern and a string
                            def extractFirstMatchingRegex(Pattern: scala.util.matching.Regex, SearchStr: String):String ={
                                val matched = Pattern.findFirstMatchIn(SearchStr)
                                var extractedVal =""
                                matched match{
                                    case Some(m) =>
                                        extractedVal = m.group(1).toString.trim
                                    case None =>
                                        extractedVal =""
                                }
                                return extractedVal
                            }
                            val tbl_name =
                            if (compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim == "SlotGame" &&
                                compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "play" &&
                                compact(render(json \ "tf")).length > 0 && compact(render(json \ "tf")).replace("\"", "").trim == "false" &&
                                compact(render(json \ "queryParams")).length > 0){
                                    "rgs_slot_req"
                            }else if(compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim == "SlotGame" &&
                                compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "play" &&
                                compact(render(json \ "tf")).length > 0 && compact(render(json \ "tf")).replace("\"", "").trim == "false" &&
                                compact(render(json \ "queryParams")).length == 0){
                                    "rgs_slot_res"
                            }else if(compact(render(json \ "cmd")).length > 0 &&
                                (compact(render(json \ "cmd")).replace("\"", "").trim == "slotSpun" || compact(render(json \ "cmd")).replace("\"", "").trim == "bonusGameExited" ) &&
                                compact(render(json \ "tf")).length > 0 && compact(render(json \ "tf")).replace("\"", "").trim == "false"){
                                    "non_rgs_msg"
                            }else if (compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "play" &&
                                compact(render(json \ "tf")).length > 0 && compact(render(json \ "tf")).replace("\"", "").trim == "false" &&
                                compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim != "SlotGame" &&
                                compact(render(json \ "outgoing")).length > 0 && compact(render(json \ "outgoing")).replace("\"", "").trim == "false" &&
                                (compact(render(json \ "g")).replace("\"", "").trim == "TableGame-BlackJack" ||
                                    compact(render(json \ "g")).replace("\"", "").trim == "VideoPoker" || compact(render(json \ "g")).replace("\"", "").trim == "TableGame-Roulette")){
                                        "rgs_game_req"
                            }else if (compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "play" &&
                                compact(render(json \ "tf")).length > 0 && compact(render(json \ "tf")).replace("\"", "").trim == "false" &&
                                compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim != "SlotGame" &&
                                compact(render(json \ "outgoing")).length > 0 && compact(render(json \ "outgoing")).replace("\"", "").trim == "true" &&
                                (compact(render(json \ "g")).replace("\"", "").trim == "TableGame-BlackJack" ||
                                compact(render(json \ "g")).replace("\"", "").trim == "VideoPoker" || compact(render(json \ "g")).replace("\"", "").trim == "TableGame-Roulette")){
                                    "rgs_game_res"
                            }else if(compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim == "BINGO" &&
                                compact(render(json \ "cmd")).length > 0 &&
                                (compact(render(json \ "cmd")).replace("\"", "").trim == "wonBingo" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "wonConsolationPrize" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "wonJackpot" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "wonHotSpot" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "endRoundBadges" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "gameData" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "openForNextRound" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "newRound" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "playerData")){
                                        "bingo"
                            }else if(compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "endRoundBadges" &&
                                (compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim == "POKER" ||
                                    compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim == "WILD_ACTION_POKER")){
                                        "poker"
                            }else{
                                    "other"
                            }
                            val this_records =
                            tbl_name match{
                                case "rgs_slot_req" =>
                                    {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                    }else{null}
                                    //Regex Pattern for transaction ID - To extract anything between the tag <TransactionId></TransactionId>
                                    //Extract transaction ID from XML Body
                                    val txnid = extractFirstMatchingRegex("<TransactionId>(.*)</TransactionId>".r, (json \ "requestBody").extract[String].trim)
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0  ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract slot variation - It is the value before underscore in game variation
                                    val slotstype = gamevariation.split("_")(0)
                                    //REGEX for extracting denomination amount - anything in between zero or more spaces after = and &
                                    //Extracted value can have a negative sign (optional), denomination(mandatory), two decimal points(optional)
                                    //Exponential-E Sign followed by either +/- ve sign and this can be followed by any number of integers [Entire exponential part is optional]
                                    //Extract Base game cost
                                    var temp1 = extractFirstMatchingRegex("&denomamount=\\s*(-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)&".r, (json \ "queryParams").extract[String].trim)
                                    val basegamecost =
                                        if (temp1 != "" && temp1.length >0){
                                            temp1.toDouble
                                        }else{0.toDouble}
                                    //REGEX for extracting number of lines played. Same as denomination amount
                                    //Extract Number of lines played
                                    temp1 = extractFirstMatchingRegex("<PatternsBet>\\s*(-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</PatternsBet>".r, (json \ "requestBody").extract[String].trim)
                                    val nooflinesplayed =
                                        if (temp1 != "" && temp1.length >0){
                                            temp1.toDouble
                                        }else{0.toDouble}
                                    //Pattern to extract bets per pattern
                                    //Extract Bets Per Pattern
                                    temp1 = extractFirstMatchingRegex("<BetPerPattern>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</BetPerPattern>".r,(json \ "requestBody").extract[String].trim)
                                    val betsPerPattern =
                                        if (temp1 != "" && temp1.length >0){
                                            temp1.toDouble
                                        }else{0.toDouble}
                                    // Line Cost = Base Game Cost * Bets Per Pattern
                                    val linecost = basegamecost*betsPerPattern
                                    // Total Bet Amount = Number of Lines Played * Lines Cost
                                    val totalbetamount = nooflinesplayed*linecost
                                    //Extract cash
                                    val cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).replace("\"", "").trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}

                                    gs+","+txnid+","+sessionid+","+userid+","+gamevariation+","+slotstype+","+
                                    basegamecost.toString+","+nooflinesplayed.toString+","+linecost.toString+","+
                                    totalbetamount.toString+","+cash.toString+","+level.toString+","+coins.toString+","+xp.toString+","+ts.toString+","+phpSid+","+currentCID
                                }
                            case "rgs_slot_res" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                        }else{null}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cash
                                    val cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experiment ID
                                    val experimentid =
                                        if (compact(render(json \ "params" \ "experimentName")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "params" \ "experimentName")).replace("\"", "").trim
                                        }else{null}
                                    //Extract experiment group
                                    val experimentgroup  =
                                        if (compact(render(json \ "params" \ "experimentGroup")).replace("\"", "").trim.length > 0  ){
                                            compact(render(json \ "params" \ "experimentGroup")).replace("\"", "").trim
                                        }else{null}
                                    //Extract transaction id, stage, next stage, game status, balance, settled, pending and payout
                                    val (txnid:String, stage:String, nextstage:String, gamestatus:String, balance:Double, settled:Double, pending:Double, payout:Double) =
                                        if(compact(render(json \"params"\"responseBody")).replace("\"", "").trim.length > 0 &&
                                            compact(render(json \"params"\"responseBody")).replace("\"", "").trim != "null"){
                                            (
                                            if (extractFirstMatchingRegex( "<TransactionId>(.*)</TransactionId>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<TransactionId>(.*)</TransactionId>".r, (json  \"params"\"responseBody").extract[String].trim)
                                            }else{null},
                                            if (extractFirstMatchingRegex( "<Stage>(.*)</Stage>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<Stage>(.*)</Stage>".r, (json  \"params"\"responseBody").extract[String])
                                            }else{null},
                                            if (extractFirstMatchingRegex( "<NextStage>(.*)</NextStage>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<NextStage>(.*)</NextStage>".r, (json  \"params"\"responseBody").extract[String].trim)
                                            }else{null},
                                            if (extractFirstMatchingRegex( "<GameStatus>(.*)</GameStatus>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<GameStatus>(.*)</GameStatus>".r, (json  \"params"\"responseBody").extract[String].trim)
                                            }else{null},
                                            if (extractFirstMatchingRegex( "<Balance>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Balance>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<Balance>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Balance>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                            }else{0.toDouble},
                                            if (extractFirstMatchingRegex( "<Settled>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Settled>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<Settled>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Settled>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                            }else{0.toDouble},
                                            if (extractFirstMatchingRegex( "<Pending>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Pending>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<Pending>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Pending>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                            }else{0.toDouble},
                                            if (extractFirstMatchingRegex( "<Payout>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Payout>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<Payout>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Payout>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                            }else{0.toDouble}
                                            )
                                        }else if(compact(render(json \ "params" \"responseObject"\"OutcomeDetail")).length > 0){
                                            (
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"TransactionId")).replace("\"", "").trim.length > 0 ){
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"TransactionId")).replace("\"", "").trim
                                            }else{null},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Stage")).replace("\"", "").trim.length > 0 ){
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Stage")).replace("\"", "").trim
                                            }else{null},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"NextStage")).replace("\"", "").trim.length > 0 ){
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"NextStage")).replace("\"", "").trim
                                            }else{null},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"GameStatus")).replace("\"", "").trim.length > 0 ){
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"GameStatus")).replace("\"", "").trim
                                            }else{null},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim !="" &&
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim.toDouble
                                            }else{0.toDouble},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim !="" &&
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim.toDouble
                                            }else{0.toDouble},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim !="" &&
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim.toDouble
                                            }else{0.toDouble},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim !="" &&
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim.toDouble
                                            }else{0.toDouble}
                                            )
                                        }else{
                                            ("","","","",0.toDouble,0.toDouble,0.toDouble,0.toDouble)
                                    }

                                    val firstWinningString =
                                        if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winning")).replace("\"", "").trim.length > 0){
                                            compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winning")).replace("\"", "").trim
                                        }else{
                                            null
                                        }

                                    val secondWinningString =
                                        if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winning")).replace("\"", "").trim.length > 0){
                                            compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winning")).replace("\"", "").trim
                                        }else{
                                            null
                                        }

                                    val firstAmount =
                                        if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winAmount")).replace("\"", "").trim.length > 0){
                                            compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winAmount")).replace("\"", "").trim
                                        }else{
                                            "0"
                                        }

                                    val secondAmount =
                                        if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winAmount")).replace("\"", "").trim.length > 0){
                                            compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winAmount")).replace("\"", "").trim
                                        }else{
                                            "0"
                                        }

                                    val firstJackpot =
                                        if ( compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0) \"jackpotID")).replace("\"", "").trim.length > 0){
                                            compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0) \"jackpotID")).replace("\"", "").trim
                                        }else{
                                            null
                                        }

                                    val secondJackpot =
                                        if ( compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1) \"jackpotID")).replace("\"", "").trim.length > 0){
                                            compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1) \"jackpotID")).replace("\"", "").trim
                                        }else{
                                            null
                                        }
                                    val jackpot:Long =
                                        if (firstWinningString == "true" && secondWinningString=="false" && firstAmount!="null"){
                                            firstAmount.toLong
                                        }else if (firstWinningString == "false" && secondWinningString=="true" && secondAmount!="null" ){
                                            secondAmount.toLong
                                        }else if (firstWinningString == "true" && secondWinningString=="true" && firstAmount!="null" && secondAmount!="null"){
                                            firstAmount.toLong+secondAmount.toLong
                                        }else{
                                            0.toLong
                                        }
                                    val jackpotid:String =
                                        if (firstWinningString == "true" && secondWinningString=="false" && firstJackpot!="null"){
                                            firstJackpot
                                        }else if (firstWinningString == "false" && secondWinningString=="true" && secondJackpot!="null"){
                                            secondJackpot
                                        }else if (firstWinningString == "true" && secondWinningString=="true" && firstJackpot!="null" && secondJackpot!="null"){
                                            firstJackpot+"&"+secondJackpot
                                        }else{
                                            null
                                        }
                                    gs+","+txnid+","+sessionid+","+userid+","+stage+","+ nextstage+","+gamestatus+","+balance.toString+","+settled.toString+","+pending.toString+","+payout.toString+","+
                                    cash.toString+","+level.toString+","+coins.toString+","+xp.toString+","+ts.toString+","+jackpot.toLong+","+experimentid+","+experimentgroup+","+jackpotid
                                }
                            case  "non_rgs_msg" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                        }else{null}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract slots type
                                    val slotstype =
                                        if (compact(render(json \"params"\"slotsType")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"slotsType")).replace("\"", "").trim
                                        }else{null}
                                    //Extract command
                                    val cmd =
                                        if (compact(render(json \ "cmd")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "cmd")).replace("\"", "").trim
                                        }else{null}
                                    //Extract line cost
                                    val linecost =
                                        if (compact(render(json \"params"\"lineCost")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json  \"params"\"lineCost")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract spins, linesplayed, lineswon, lineCost:linewinnings, freespinwinnings, winnings
                                    val (spins, linesplayed:Long, lineswon, lineCost:Long, linewinnings:Double, freespinwinnings:Double, winnings:Double) =
                                        if (compact(render(json \"cmd")).replace("\"", "").trim.length > 0 &&
                                            compact(render(json \"cmd")).replace("\"", "").trim == "slotSpun"){
                                                (//Spins
                                                1,
                                                //lines played
                                                if (compact(render(json \"params"\"linesPlayed")).replace("\"", "").trim.length > 0 ){
                                                    compact(render(json  \"params"\"linesPlayed")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                //lines won
                                                if (compact(render(json \"params"\"winLines")).replace("\"", "").trim.length > 0 &&
                                                    (json \"params"\"winLines").extract[List[JValue]].length != 0 ){
                                                        (json \"params"\"winLines").extract[List[JValue]].length
                                                }else{0},
                                                //line cost
                                                if (compact(render(json \"params"\"lineCost")).replace("\"", "").trim.length > 0 ){
                                                    compact(render(json  \"params"\"lineCost")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                //line winnings
                                                if (compact(render(json  \"params"\"winnings")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"winnings")).replace("\"", "").trim.length > 0){
                                                        compact(render(json  \"params"\"winnings")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble},
                                                //free spin winnings
                                                if (compact(render(json  \"params"\"totalFreeSpinWinnings")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"totalFreeSpinWinnings")).replace("\"", "").trim.length > 0){
                                                        compact(render(json  \"params"\"totalFreeSpinWinnings")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble},
                                                //winnings
                                                if (compact(render(json  \"params"\"totalWinnings")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"totalWinnings")).replace("\"", "").trim.length > 0){
                                                        compact(render(json  \"params"\"totalWinnings")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble})
                                            }else{
                                                (0,0.toLong,0,0.toLong,0.toDouble,0.toDouble,0.toDouble)
                                            }
                                    //Extract bet amount
                                    val betamount = linesplayed * lineCost
                                    //Extract bet lost
                                    val betlost = betamount - linewinnings
                                    //Extract jackpot
                                    val jackpot =
                                    if (compact(render(json \"cmd")).replace("\"", "").trim.length > 0 &&
                                        compact(render(json \"cmd")).replace("\"", "").trim == "slotSpun" &&
                                        compact(render(json \"params"\"wonJackpot")).replace("\"", "").trim.length > 0 &&
                                        compact(render(json \"params"\"wonJackpot")).replace("\"", "").trim == "true" &&
                                        compact(render(json \"params"\"jackpot")).replace("\"", "").trim !="" &&
                                        compact(render(json \"params"\"jackpot")).replace("\"", "").trim.length > 0){
                                            compact(render(json \"params"\"jackpot")).replace("\"", "").trim.toDouble
                                    }else{0.toDouble}
                                    //Extract bonus winnings
                                    val bonuswinnings =
                                        if (compact(render(json \"cmd")).replace("\"", "").trim.length > 0 &&
                                            compact(render(json \"cmd")).replace("\"", "").trim == "bonusGameExited" &&
                                            compact(render(json \"params"\"totalWinnings")).replace("\"", "").trim.length > 0 &&
                                            compact(render(json \"params"\"totalWinnings")).replace("\"", "").trim == "true"){
                                                compact(render(json \"params"\"totalWinnings")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract end cash
                                    val endcash =
                                        if (compact(render(json  \"params"\"userData"\"cash")).replace("\"", "").trim !="" &&
                                            compact(render(json  \"params"\"userData"\"cash")).replace("\"", "").trim.length > 0){
                                                compact(render(json  \"params"\"userData"\"cash")).replace("\"", "").trim.toDouble
                                        }else{0.toDouble}
                                    //Extract begining cash
                                    val begincash = endcash + (betamount - (if(linewinnings>0) linewinnings else winnings))
                                    //Extract level
                                    val level =
                                        if (compact(render(json \"params"\"userData"\"level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"userData"\"level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \"params"\"userData"\"coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"userData"\"coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract rockets
                                    val rockets =
                                        if (compact(render(json  \"params"\"rockets")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json  \"params"\"rockets")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract xp
                                    val xp =
                                        if (compact(render(json  \"params"\"userData"\"xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json  \"params"\"userData"\"xp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract ts
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    //Return required fields
                                    gs+","+sessionid+","+userid+","+ gamevariation+","+ slotstype+","+ cmd+","+linecost.toString+","+spins.toString+","+linesplayed.toString+","+
                                    lineswon.toString+","+ betamount.toString+","+ betlost.toString+","+ bonuswinnings.toString+","+jackpot.toString+","+freespinwinnings.toString+","+
                                    linewinnings.toString+","+winnings.toString+","+begincash.toString+","+endcash.toString+","+level.toString+","+coins.toString+","+
                                    rockets.toString+","+xp.toString+","+ts.toString+","+phpSid+","+currentCID
                                }
                            case "rgs_game_req" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                    }else{null}
                                    //Regex Pattern for transaction ID - To extract anything between the tag <TransactionId></TransactionId>
                                    //Extract transaction ID from XML Body
                                    val txnid = extractFirstMatchingRegex("<TransactionId>(.*)</TransactionId>".r, compact(render(json \ "requestBody")).trim)
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game type
                                    val gametype =
                                        if (compact(render(json \ "g")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "g")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract base line cost
                                    val baselinecost =
                                        if (extractFirstMatchingRegex("&denomamount=\\s*(-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)&".r, compact(render(json \ "queryParams")).trim).length > 0 ){
                                            extractFirstMatchingRegex("&denomamount=\\s*(-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)&".r,(json \ "queryParams").extract[String].trim).toDouble
                                        }else{0.toDouble}
                                    //Extract cash
                                    val cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                    }else{"0"}
                                    gs+","+txnid+","+sessionid+","+userid+","+gametype+","+gamevariation+","+baselinecost.toString+","+cash.toString+","+level.toString+","+
                                    coins.toString+","+xp.toString+","+ts.toString+","+phpSid+","+currentCID
                                }
                            case "rgs_game_res" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                        }else{null}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract transaction id, stage, next stage, game status, balance, settled, pending and payout
                                    val (txnid:String, gamestatus:String, balance:Long, settled:Long, pending:Long, payout:Long) =
                                        if(compact(render(json \"params"\"responseBody")).replace("\"", "").trim.length > 0 &&
                                            compact(render(json \"params"\"responseBody")).replace("\"", "").trim != "null"){
                                            (
                                                if (extractFirstMatchingRegex( "<TransactionId>(.*)</TransactionId>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                    extractFirstMatchingRegex( "<TransactionId>(.*)</TransactionId>".r, (json  \"params"\"responseBody").extract[String].trim)
                                                }else{""},
                                                if (extractFirstMatchingRegex( "<GameStatus>(.*)</GameStatus>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                    extractFirstMatchingRegex( "<GameStatus>(.*)</GameStatus>".r, (json  \"params"\"responseBody").extract[String].trim)
                                                }else{""},
                                                if (extractFirstMatchingRegex( "<Balance>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Balance>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                    extractFirstMatchingRegex( "<Balance>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Balance>".r, (json  \"params"\"responseBody").extract[String].trim).toLong
                                                }else{0.toLong},
                                                if (extractFirstMatchingRegex( "<Settled>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Settled>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                    extractFirstMatchingRegex( "<Settled>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Settled>".r, (json  \"params"\"responseBody").extract[String].trim).toLong
                                                }else{0.toLong},
                                                if (extractFirstMatchingRegex( "<Pending>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Pending>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                    extractFirstMatchingRegex( "<Pending>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Pending>".r, (json  \"params"\"responseBody").extract[String].trim).toLong
                                                }else{0.toLong},
                                                if (extractFirstMatchingRegex( "<Payout>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Payout>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                    extractFirstMatchingRegex( "<Payout>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Payout>".r, (json  \"params"\"responseBody").extract[String].trim).toLong
                                                }else{0.toLong}
                                            )
                                        }else if(compact(render(json \ "params" \"responseObject"\"OutcomeDetail")).length > 0){
                                            (
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"TransactionId")).replace("\"", "").trim.length > 0 ){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"TransactionId")).replace("\"", "").trim
                                                }else{""},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"GameStatus")).replace("\"", "").trim.length > 0 ){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"GameStatus")).replace("\"", "").trim
                                                }else{""},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim.length > 0){
                                                        compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim.length > 0){
                                                        compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim.length > 0){
                                                        compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim.length > 0){
                                                        compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim.toLong
                                                }else{0.toLong}
                                            )
                                        }else{
                                            ("","",0.toLong,0.toLong,0.toLong,0.toLong)
                                        }
                                    //Extract cash
                                    val cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    gs+","+txnid+","+sessionid+","+userid+","+gamestatus+","+balance.toString+","+settled.toString+","+pending.toString+","+
                                    payout.toString+","+cash.toString+","+level.toString+","+coins.toString+","+xp.toString+","+ts.toString
                                }
                            case "bingo" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                        }else{null}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game type
                                    val gametype =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (gametype != null && gametype!= "null" && gametype.length>0){
                                            gametype.trim.substring(0, gametype.trim.lastIndexOf(" "))
                                        }else{null}
                                    //Extract play type
                                    val playtype =
                                        if (gametype != null && gametype!= "null" && gametype.length>0){
                                            gametype.trim.substring(gametype.trim.indexOf("_")+1, gametype.trim.lastIndexOf("_"))
                                        }else{null}
                                    //Extract round id
                                    val roundid =
                                        if (compact(render(json \ "roundId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "roundId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cmd
                                    val cmd =
                                        if (compact(render(json \ "cmd")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "cmd")).replace("\"", "").trim
                                        }else{null}
                                    //Extract number of cards played
                                    val noofcardsplayed =
                                        if (compact(render(json \"params"\"purchasedCardCount")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"purchasedCardCount")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract card cost
                                    val cardcost =
                                        if (compact(render(json \"params"\"cardcost")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"cardcost")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract bingo winnings
                                    val bingowinnings =
                                        if (cmd =="wonBingo" && compact(render(json \ "params"\"winnings")).replace("\"", "").trim.length > 0){
                                            compact(render(json \ "params"\"winnings")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract hot chip winnings
                                    val hotchipwinnings =
                                        if (cmd =="wonHotSpot" && compact(render(json \ "params"\"amount")).replace("\"", "").trim.length > 0){
                                            compact(render(json \ "params"\"amount")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract consolation winnings
                                    val consolationwinnings =
                                        if (cmd =="wonConsolationPrize" && compact(render(json \ "params"\"amount")).replace("\"", "").trim.length > 0){
                                            compact(render(json \ "params"\"amount")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract jackpot winnings
                                    val jackpotwinnings =
                                        if (cmd =="wonJackpot" && compact(render(json \ "params"\"winnings")).replace("\"", "").trim.length > 0){
                                            compact(render(json \ "params"\"winnings")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract is quit game
                                    val isquitgame =
                                        if (cmd =="endRoundBadges"){
                                            "false"
                                        }else{null}
                                    //Exract autodaub
                                    val autodaub =
                                        if (cmd == "gameData" &&
                                            compact(render(json \ "params"\"autoPilot")).replace("\"", "").trim.length > 0){
                                                compact(render(json \"params"\"autoPilot")).replace("\"", "").trim
                                        }else if (cmd =="newRound" &&
                                                compact(render(json \ "params"\"autoDaub")).replace("\"", "").trim.length > 0){
                                                    compact(render(json \ "params"\"autoDaub")).replace("\"", "").trim
                                        }else{null}
                                    //Extract pattern
                                    val pattern_str =
                                        if ((cmd  == "openForNextRound" ||
                                            cmd == "newRound") &&
                                            compact(render(json \"params"\"pattern")).replace("\"", "").trim.length > 0){
                                                compact(render(json \"params"\"pattern")).replace("\"", "").trim
                                        }else{null}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    gs+","+sessionid+","+userid+","+gamevariation+","+gametype+","+playtype+","+roundid+","+cmd+","+noofcardsplayed.toString+","+
                                    cardcost.toString+","+bingowinnings.toString+","+hotchipwinnings.toString+","+consolationwinnings.toString+","+jackpotwinnings.toString+","+
                                    isquitgame+","+autodaub+","+pattern_str+","+ts.toString+","+phpSid+","+currentCID
                                }
                            case "poker" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                        }else{null}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game type
                                    val gametype =
                                        if (compact(render(json \ "g")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "g")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract total bet amount
                                    val totalbetamount =
                                        if (compact(render(json \"params"\"betInRound")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"betInRound")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract total amount won
                                    val totalamountwon =
                                        if (compact(render(json \"params"\"winInRound")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"winInRound")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract total amount lost
                                    val totalamountlost = totalbetamount - totalamountwon
                                    //Set Number of rounds
                                    val noofrounds=1
                                    //Set number of rounds won
                                    val noofroundswon = if(totalamountwon>0) 1 else 0
                                    //Extract end cash
                                    val end_cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract beg cash
                                    val beg_cash = end_cash + totalbetamount - totalamountwon
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    gs+","+sessionid+","+userid+","+gametype+","+gamevariation+","+totalbetamount.toString+","+totalamountwon.toString+","+totalamountlost.toString+","+
                                    noofrounds.toString+","+noofroundswon.toString+","+beg_cash.toString+","+end_cash.toString+","+level.toString+","+coins.toString+","+ts.toString+","+
                                    phpSid+","+currentCID
                                }
                            case default =>
                                {
                                    "not yet parsed"
                                }
                            }
                            (tbl_name,this_records)
            }).saveAsNewAPIHadoopFile("s3a://ddi-business-intelligence/emr_spark_testing_prod/sample/", classOf[String], classOf[String], classOf[TextOutputFormat[String, String]])
