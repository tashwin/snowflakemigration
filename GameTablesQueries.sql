set hive.optimize.s3.query=true;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition=true;
set hive.enforce.bucketing = true;

create database if not exists ddi_tables;
use ddi_tables;
drop table if exists map_rgs_slot_request;
create external table if not exists map_rgs_slot_request
                (gs string,
                txnid string,
                sessionid string,
                userid string,
                gamevariation string,
                slotstype string,
                basegamecost string,
                nooflinesplayed string,
                linecost string,
                totalbetamount string,
                cash bigint,
                level bigint,
                coins bigint,
                xp bigint,
                ts bigint,
                phpSid string,
                currentCID string)
PARTITIONED BY (year int, month int, day int, hour int)
row format delimited fields terminated by ',' LINES TERMINATED BY '\n'
LOCATION 's3n://ddi-business-intelligence/emr_spark_testing/rgs_slot_req/';

drop table if exists map_rgs_slot_response;
create external table if not exists map_rgs_slot_response
                (gs string,
                txnid string,
                sessionid string,
                userid string,
                stage string,
                nextstage string,
                gamestatus string,
                balance DOUBLE,
                settled DOUBLE,
                pending DOUBLE,
                payout DOUBLE,
                cash bigint,
                level bigint,
                coins bigint,
                xp bigint,
                ts bigint,
                jackpot bigint,
                experimentid string,
                experimentgroup string,
                jackpotid string)

PARTITIONED BY (year int, month int, day int, hour int)
row format delimited fields terminated by ',' LINES TERMINATED BY '\n'
LOCATION 's3n://ddi-business-intelligence/emr_spark_testing/rgs_slot_res/';

drop table if exists map_non_rgs_slot;
create external table if not exists map_non_rgs_slot
                (gs string,
                sessionid string,
                userid string,
                gamevariation string,
                slotstype string,
                cmd string,
                linecost bigint,
                spins bigint,
                linesplayed bigint,
                lineswon bigint,
                betamount bigint,
                betlost bigint,
                bonuswinnings bigint,
                jackpot double,
                freespinwinnings double,
                linewinnings double,
                winnings double,
                begincash  double,
                endcash double,
                level bigint,
                coins bigint,
                rockets bigint,
                xp bigint,
                ts bigint,
                phpSid string,
                currentCID string)
PARTITIONED BY (year int, month int, day int, hour int)
row format delimited fields terminated by ',' LINES TERMINATED BY '\n'
LOCATION 's3n://ddi-business-intelligence/emr_spark_testing/non_rgs_slot/';

drop table if exists map_rgs_game_req;
create external table if not exists map_rgs_game_req
                (gs string,
                txnid string,
                sessionid string,
                userid string,
                gametype string,
                gamevariation string,
                baselinecost double,
                cash bigint,
                level bigint,
                coins bigint,
                xp bigint,
                ts bigint,
                phpSid string,
                currentCID string)
PARTITIONED BY (year int, month int, day int, hour int)
row format delimited fields terminated by ',' LINES TERMINATED BY '\n'
LOCATION 's3n://ddi-business-intelligence/emr_spark_testing/rgs_game_req/';

drop table if exists map_rgs_game_res;
create external table if not exists map_rgs_game_res
                (gs string,
                txnid string,
                sessionid string,
                userid string,
                gamestatus string,
                balance bigint,
                settled bigint,
                pending bigint,
                payout bigint,
                cash bigint,
                level bigint,
                coins bigint,
                xp bigint,
                ts bigint)
PARTITIONED BY (year int, month int, day int, hour int)
row format delimited fields terminated by ',' LINES TERMINATED BY '\n'
LOCATION 's3n://ddi-business-intelligence/emr_spark_testing/rgs_game_res';

drop table if exists map_bingo;
create external table if not exists map_bingo
                (gs string,
                sessionid string,
                userid string,
                gamevariation string,
                gametype string,
                playtype string,
                roundid string,
                cmd string,
                noofcardsplayed bigint,
                cardcost bigint,
                bingowinnings bigint,
                hotchipwinnings bigint,
                consolationwinnings bigint,
                jackpotwinnings bigint,
                isquitgame string,
                autodaub string,
                pattern_str string,
                ts bigint,
                phpSid string,
                currentCID string)
PARTITIONED BY (year int, month int, day int, hour int)
row format delimited fields terminated by ',' LINES TERMINATED BY '\n'
LOCATION 's3n://ddi-business-intelligence/emr_spark_testing/bingo';

drop table if exists map_poker;
create external table if not exists map_poker
                (gs string,
                sessionid string,
                userid string,
                gametype string,
                gamevariation string,
                totalbetamount bigint,
                totalamountwon bigint,
                totalamountlost bigint,
                noofrounds bigint,
                noofroundswon bigint,
                beg_cash bigint,
                end_cash bigint,
                level bigint,
                coins bigint,
                ts bigint,
                phpSid string,
                currentCID string)
PARTITIONED BY (year int, month int, day int, hour int)
row format delimited fields terminated by ',' LINES TERMINATED BY '\n'
LOCATION 's3n://ddi-business-intelligence/emr_spark_testing/poker';

msck repair table map_rgs_slot_request;
msck repair table map_rgs_slot_response;
msck repair table map_non_rgs_slot;
msck repair table map_rgs_game_req;
msck repair table map_rgs_game_res;
msck repair table map_bingo;
msck repair table map_poker;
