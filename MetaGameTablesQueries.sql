use ddi_tables;
drop table if exists map_accepted_gifts;
create external table if not exists map_accepted_gifts
                (gift_id string,
                recipient_user_id string,
                ts bigint)
PARTITIONED BY (year int, month int, day int, hour int)
row format delimited fields terminated by ',' LINES TERMINATED BY '\n'
LOCATION 's3n://ddi-business-intelligence/emr_spark_testing/accepted_gifts';

drop table if exists map_gift_activity;
create external table if not exists map_gift_activity
                (userid string,
                sessionid string,
                cash  bigint,
                level  bigint,
                coins  bigint,
                wheelindex  bigint,
                xp  bigint,
                ts  bigint,
                phpSid string,
                currentCID string)
PARTITIONED BY (year int, month int, day int, hour int)
row format delimited fields terminated by ',' LINES TERMINATED BY '\n'
LOCATION 's3n://ddi-business-intelligence/emr_spark_testing/gift_activity';

drop table if exists map_poker_financials;
create external table if not exists map_poker_financials
                (sessionid  string,
                userid string,
                game string,
                gamevariation string,
                level string,
                xp string,
                cash string,
                coins string,
                roundid string,
                wintotal string,
                bettotal string,
                raketotal string,
                ts string,
                phpSid string,
                currentCID string)
PARTITIONED BY (year int, month int, day int, hour int)
row format delimited fields terminated by ',' LINES TERMINATED BY '\n'
LOCATION 's3n://ddi-business-intelligence/emr_spark_testing/poker_financials';

drop table if exists map_rgs_slots_jackpots;
create external table if not exists map_rgs_slots_jackpots
                (ts bigint,
                userid string,
                sessionid string,
                phpSid string,
                gs string,
                txnid string,
                experimentGroup string,
                experimentName string,
                gameId string,
                growthPerSecond bigint,
                hot string,
                jackpotID string,
                jackpotInstanceId string,
                minBetAmount bigint,
                newValue bigint,
                skinID string,
                uniqueJackpotID string,
                value bigint,
                winAmount bigint,
                experimentName string)
PARTITIONED BY (year int, month int, day int, hour int)
row format delimited fields terminated by ',' LINES TERMINATED BY '\n'
LOCATION 's3n://ddi-business-intelligence/emr_spark_testing/rgs_slots_jackpots';

msck repair table map_accepted_gifts;
msck repair table map_gift_activity;
msck repair table map_poker_financials;
msck repair table map_rgs_slots_jackpots;

add jar /usr/lib/hive/lib/hive-contrib.jar;
CREATE TEMPORARY FUNCTION row_sequence as 'org.apache.hadoop.hive.contrib.udf.UDFRowSequence';

drop table if exists gift_weightage_temp;
CREATE external TABLE if not exists gift_weightage_temp
(data string)
location 's3n://ddi-business-intelligence/emr_spark_testing/gift_weights/';

create table if not exists gift_weightage as
select
    row_sequence() as wheelindex,
    get_json_object(b.info_array,'$.type') as type,
    cast(get_json_object(b.info_array,'$.value') as bigint) as value,
    cast(get_json_object(b.info_array,'$.weight') as bigint) as weight
    from
    (
    select explode (split(regexp_replace(regexp_replace(data,'\\}\\,\\{','\\}\\;\\{'),'\\[|\\]',''),'\\;'))as info_array
    from gift_weightage_temp
    )b;

drop table if exists gift_weightage_temp;
