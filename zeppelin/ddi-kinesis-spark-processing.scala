/* ddi-kinesis-spark-processing.scala */

%dep
z.reset
z.load("org.apache.spark:spark-streaming-kinesis-asl_2.10:1.6.1")
z.load("/usr/lib/spark/extras/lib/json4s-native_2.10.jar")
z.load("/usr/lib/spark/extras/lib/json4s-jackson_2.10.jar")

//Import all Amazon AWS Libraries -- Kinesis, Cloud Watch, & Basic Authentication
import com.amazonaws.auth.{BasicAWSCredentials, DefaultAWSCredentialsProviderChain}
import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.kinesis.AmazonKinesisClient
import com.amazonaws.services.kinesis.model.PutRecordRequest
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient
import com.amazonaws.services.cloudwatch.model.{ StandardUnit, PutMetricDataRequest, MetricDatum, Dimension, StatisticSet }
//Import Hadoop Libraries
import org.apache.hadoop.conf.{Configurable, Configuration}
import org.apache.hadoop.util.Progress
import org.apache.hadoop.mapreduce._
import org.apache.hadoop.io.{DataInputBuffer, NullWritable, Text}
import org.apache.hadoop.fs.{FileSystem,Path}
import org.apache.hadoop.mapreduce.lib.output.{TextOutputFormat, LazyOutputFormat, MultipleOutputs}
import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat
import org.apache.hadoop.mapreduce.task.{ReduceContextImpl, TaskAttemptContextImpl}
import org.apache.hadoop.mapred.RawKeyValueIterator
import org.apache.hadoop.mapreduce.counters.GenericCounter
import org.apache.hadoop.io.compress._
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl.DummyReporter
//Import required Spark Libraries
import org.apache.spark._
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext, Time}
import org.apache.spark.streaming.dstream.DStream.toPairDStreamFunctions
import org.apache.spark.streaming.kinesis.KinesisUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SQLContext, Row, DataFrame}
//Import Java Libraries
import java.net.URI
import java.util.concurrent.TimeUnit
import java.util.{Date, TimeZone, Calendar}
//Import Scala Libraries
import scala.collection.JavaConversions._
import scala.reflect.ClassTag
//Import Json4s libraries
import org.json4s.jackson.JsonMethods
import org.json4s.jackson.JsonMethods._
import org.json4s.JsonAST._
import org.json4s.DefaultFormats
import org.json4s.JsonAST.JValue
import org.json4s.JsonAST.JNothing
import org.json4s.JsonAST.JString
import org.json4s.native.Serialization
import org.json4s.native.Serialization.{read, write}
//Import all the logging libraries
import org.apache.log4j.{Level, Logger}
import com.typesafe.config.{Config, ConfigFactory}
//Import Google IO
import com.google.common.io.Files

object DDI_Stream_Processing extends Serializable{

    //val sc = new SparkContext(new SparkConf().setAppName("DDI Streaming PROD"))

    var thisTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
    /*This method is used to get UTC time and return it as a formatted string.
    The format of the string returned by this method is
    year/month/date/hour(24 hours)
    Since UTC month begins at 0, its value has been incremented by 1.
    This methos is useful to determine the output path to store data*/
    def getOutputPath():String = {
        val thisTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        val path1 = "%04d/%02d/%02d/%02d/" format (thisTime.get(Calendar.YEAR), thisTime.get(Calendar.MONTH)+1,thisTime.get(Calendar.DATE),thisTime.get(Calendar.HOUR_OF_DAY))
        return path1
    }
    /*This method is used to return several date components.
    This method accepts the following arguments
    1. Calander Object - java.util.Calendar
    2. Component to be fetched - String (year/month/day/hour)*/
    def returnDateComponents(cal: java.util.Calendar, toFind: String):String = {
        val retString =  toFind match{
            case "year" => "%04d" format (thisTime.get(Calendar.YEAR))
            case "month" => "%02d" format (thisTime.get(Calendar.MONTH)+1)
            case "day" => "%02d" format (thisTime.get(Calendar.DATE))
            case "hour" =>"%02d" format (thisTime.get(Calendar.HOUR_OF_DAY))
            case _ => ""
        }
        retString
    }
    /*This function is used to return local host name as a string.
    This method doesn't accept any arguments*/
    def getHostName():String={
        return java.net.InetAddress.getLocalHost().getHostName()
    }

    /*This function creates required dimension object to be passed to cloudwatch.
    This method accepts two arguments which are arrays of string.
    List 1 (L1) - Contains Dimenstion Name
    List 2 (L2) - Contains Dimension Values
    List returned is of the type scala.collection.immutable.Iterable[com.amazonaws.services.cloudwatch.model.Dimension]
    This return type is equivalent of java datatype java.util.Collection<Dimension>.*/
    def returnDimensionsForCloudWatch(L1: Array[String], L2: Array[String]):scala.collection.immutable.Iterable[com.amazonaws.services.cloudwatch.model.Dimension] ={
        var dimensions = scala.collection.mutable.MutableList[Dimension]()
        for(i <- 0 until L1.length){
            dimensions += (new Dimension).withName(L1(i)).withValue(L2(i))
        }
        val dimsArg:scala.collection.immutable.Iterable[com.amazonaws.services.cloudwatch.model.Dimension] = dimensions.toList
        return dimsArg
    }
    /*This method is used to report metrics to amazon cloudwatch,
    This method doesn't return anything.
    This method accepts the following parameters
    1. metricData - of type MetricDatum
    2.nameSpace - String
    3. AWS Cloud Watch Client - AmazonCloudWatchClient*/
    def send(metricData: MetricDatum, nameSpace: String, awsCloudWatchClient:AmazonCloudWatchClient) {
        try {
        val metricRequest = (new PutMetricDataRequest).withMetricData(metricData).withNamespace(nameSpace)
        awsCloudWatchClient.putMetricData(metricRequest)
        }
        catch { case e: Throwable =>
        println("Exception")
        }
    }
    /*This method creates metric datum and calls the method defined above to report metric to cloudwatch.This method returns nothing. This method accepts the following arguments.
    1. Name of the metric - String
    2. Value of the metric - Double
    3. timestamp in milliseconds - long
    4. Dimension associated with the metric -  scala.collection.immutable.Iterable[com.amazonaws.services.cloudwatch.model.Dimension]
    5. Name space - String
    6. AWS Cloud Watch Client - AmazonCloudWatchClien*/
    def sendValue(name: String, value: Double, timestamp: Long, dimen: scala.collection.immutable.Iterable[com.amazonaws.services.cloudwatch.model.Dimension], nameSpace: String, awsCloudWatchClient:AmazonCloudWatchClient)     {
        if (value != 0d) {
        val metricData = (new MetricDatum()).withDimensions(dimen)
                                          .withMetricName(name)
                                          .withValue(value)
                                          .withTimestamp(new Date(timestamp))
                                          .withUnit(StandardUnit.None)
        send(metricData, nameSpace, awsCloudWatchClient)
        }
    }
    /*This method check if a configuration file in S3 bucket exists.
    If the file exists, this method returns true
    Else this method returns false
    This method accepts the following arguments
    1. Bucket Name - String
    2. File Name - String
    */
    def checkIfS3PathExists(BucketName: String, FileName: String):Boolean = {
        if (FileSystem.get(new URI(BucketName), sc.hadoopConfiguration).exists(new Path(BucketName+FileName))){
            return true
        }else{
            return false
        }
    }
    /*This method check if a configuration exists in a config file.
    If the file exists, this method returns true
    Else this method returns false
    This method accepts the following arguments
    1. Loaded Configuration File - com.typesafe.config.Config
    2. Configuration/Property Name - String
    */
    def checkIfPathExistsInConfigFile(config: com.typesafe.config.Config, propName: String):Boolean = {
        if (config.hasPath(propName)){
            return true
        }else{
            return false
        }
    }
    /*This method is used to load a configuration file from a S3 bucket.
    This method returns an object of the type com.typesafe.config.Config
    This method accepts Configuration/Property File (in S3) Name - String
    */
    def loadConfigFileFromS3(S3Path: String): com.typesafe.config.Config = {
        return ConfigFactory.parseString(sc.textFile(S3Path).collect().mkString("\n"))
    }
    /*This method is used to return a configuration/property in a config file.
    This method accepts the following arguments
    1. Loaded Configuration File - com.typesafe.config.Config
    2. Configuration/Property Name - String*/
    def getPropertyFromConfigFile(config: com.typesafe.config.Config, propName: String): String= {
        return config.getString(propName)
    }
    /*This method will return an array of properties from a configuration file.
    This method accepts the following arguments
    1. Bucket Name - String
    2. Config File Name - String
    3. Configurations to load - Array of String*/
    def LoadAllConfigurationParams(BucketName: String, ConfigFileName: String, confArray:Array[String]):Array[String] = {
        val ValueForConfParams:Array[String] = new Array[String](confArray.length)
        if (checkIfS3PathExists(BucketName, ConfigFileName)){
            val config = loadConfigFileFromS3(BucketName+ConfigFileName)
            for(i <- 0 until confArray.length){
                if (checkIfPathExistsInConfigFile(config,confArray(i))){
                    ValueForConfParams(i) = getPropertyFromConfigFile(config,confArray(i))
                }else{
                    println("Log Error -- Property Not Found "+confArray(i))
                }
            }
        }else{
            println("Log error -- Conf File Not Found")
        }
        return ValueForConfParams
    }

    def getHadoopBaseName(hconf: org.apache.hadoop.conf.Configuration, pathName: String):String={
        hconf.set("mapreduce.output.basename", getHostName()+"_"+System.currentTimeMillis.toString+"_"+scala.util.Random.nextInt(100000).toString)
        thisTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        val rgs_slot_output = "s3a://ddi-business-intelligence/emr_spark_testing_prod/"+pathName+"/year="+returnDateComponents(thisTime,"year")+"/month="+returnDateComponents(thisTime,"month")+"/day="+returnDateComponents(thisTime,"day")+"/hour="+returnDateComponents(thisTime,"hour")+"/"
        return rgs_slot_output
    }

    var ssc:org.apache.spark.streaming.StreamingContext = null

    def main(args: Array[String]): Unit = {
        val BucketName:String = "s3n://ddi-business-intelligence/emr_spark_testing_prod/"
        val ConfigFileName:String = "Kinesis.conf"
        val PropertiesToBeLoaded = Array("aws.endPointURL","aws.regionName","aws.credentialsError",
                                 "aws.batchInterval","aws.repartitions","SFS_Prod.streamInfo.streamName",
                                 "SFS_Prod.streamInfo.appName","SFS_Prod.dataStoreLocation.unProcessedS3URL",
                                 "SFS_Prod.dataStoreLocation.processedS3URL","SFS_Prod.cloudWatchMetrics.cloudWatchMetricName",
                                 "aws.logLocation")
        //Load all the configurations from conf file in S3 Location
        val LoadedConfig = LoadAllConfigurationParams(BucketName, ConfigFileName, PropertiesToBeLoaded)

        val endPointURL = LoadedConfig(0)
        val regionName = LoadedConfig(1)
        val credentialsErrorURL = LoadedConfig(2)
        val batchInterval = Seconds(LoadedConfig(3).toInt)
        val repartitions = LoadedConfig(4).toInt
        val streamName = LoadedConfig(5)
        val appName = LoadedConfig(6)
        val unprocessedS3URL = LoadedConfig(7)
        val processedS3URL = LoadedConfig(8)
        val cloudWatchMetricName = LoadedConfig(9)
        val logFileLocation = LoadedConfig(10)+"/ddi-streaming"+getHostName()+".log"

        //Get Default Credentials for EMR
        val credentials = new DefaultAWSCredentialsProviderChain().getCredentials()
        require(credentials != null,"No AWS credentials found. Please specify credentials using one of the methods specified in " +credentialsErrorURL)
        //Create an Instance for Kinesis Client
        val kinesisClient = new AmazonKinesisClient(credentials)
        //Create an Instance for Amazon CloudWatch Client
        val awsCloudWatchClient   = new AmazonCloudWatchClient(credentials)
        //This part of the code is to create dimensions to upload to cloud watch
        val L1 = Array("Host","StreamName")
        val L2 = Array(getHostName(),streamName)
        val dims = returnDimensionsForCloudWatch(L1, L2)
        val dims1 = returnDimensionsForCloudWatch(L1.slice(1, L1.length),L2.slice(1, L2.length))

        //This part of the code is used to initialize kinesis streams and process streaming data
        kinesisClient.setEndpoint(endPointURL)
        //Get number of shards
        val numShards = kinesisClient.describeStream(streamName).getStreamDescription().getShards().size
        //Create Spark streaming context
        ssc = new StreamingContext(sc, batchInterval)
        //Create stream for all shards
        val kinesisStreams = (0 until numShards).map { i =>
            KinesisUtils.createStream(ssc, appName, streamName, endPointURL, regionName, InitialPositionInStream.LATEST, batchInterval, StorageLevel.MEMORY_AND_DISK_SER_2)
        }

        val hadoopConf = new Configuration()
        hadoopConf.set("mapreduce.output.fileoutputformat.compress","true")
        hadoopConf.set("mapreduce.output.fileoutputformat.compress.codec","org.apache.hadoop.io.compress.GzipCodec")
        //val job = Job.getInstance();
        //LazyOutputFormat.setOutputFormatClass(job, classOf[TextOutputFormat[NullWritable, String]])

        //Combine all the individual streams
        val unionStreams = ssc.union(kinesisStreams)
        //Translate byte streams to string
        //val parsedStream = unionStreams.map(byteArray => new String(byteArray))

        //Write Unprocessed Data into S3
        //val unprocessedStream = parsedStream

        //Unprocessed Data
        /*unionStreams
        .foreachRDD{ (rdd: RDD[Array[Byte]], time: Time) =>{
                hadoopConf.set("mapreduce.output.basename", System.currentTimeMillis.toString)
                thisTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
                val outputFolder = unprocessedS3URL+getOutputPath()
                val rdd_count = rdd.count()
                rdd.map(byteArray => new String(byteArray)).map(str => (null, str)).saveAsNewAPIHadoopFile(s"$outputFolder", classOf[NullWritable], classOf[String], classOf[TextOutputFormat[NullWritable, String]], hadoopConf)
                sendValue(cloudWatchMetricName, rdd_count, System.currentTimeMillis, dims, appName, awsCloudWatchClient)
                sendValue(cloudWatchMetricName, rdd_count, System.currentTimeMillis, dims1, appName, awsCloudWatchClient)
            }
        }*/

        //Parse streams according to games
        unionStreams.foreachRDD{ (rdd: RDD[Array[Byte]], time: Time) =>{

        hadoopConf.set("mapreduce.output.basename", System.currentTimeMillis.toString)
        val outputFolder = unprocessedS3URL+getOutputPath()
        val str_rdd = rdd
        str_rdd.map(byteArray => new String(byteArray)).map(str => (null, str)).saveAsNewAPIHadoopFile(s"$outputFolder", classOf[NullWritable], classOf[String], classOf[TextOutputFormat[NullWritable, String]], hadoopConf)

        val transformedRDD =
            rdd
            .map({byteArray => new String(byteArray)})
            .map({line => parse(line)})
            .map(json =>  {
                            implicit val formats = DefaultFormats
                            //This method returns the first match for a pattern and a string
                            def extractFirstMatchingRegex(Pattern: scala.util.matching.Regex, SearchStr: String):String ={
                                val matched = Pattern.findFirstMatchIn(SearchStr)
                                var extractedVal =""
                                matched match{
                                    case Some(m) =>
                                        extractedVal = m.group(1).toString.trim
                                    case None =>
                                        extractedVal =""
                                }
                                return extractedVal
                            }
                            val tbl_name =
                            if (compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim == "SlotGame" &&
                                compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "play" &&
                                compact(render(json \ "tf")).length > 0 && compact(render(json \ "tf")).replace("\"", "").trim == "false" &&
                                compact(render(json \ "queryParams")).length > 0){
                                    "rgs_slot_req"
                            }else if(compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim == "SlotGame" &&
                                compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "play" &&
                                compact(render(json \ "tf")).length > 0 && compact(render(json \ "tf")).replace("\"", "").trim == "false" &&
                                compact(render(json \ "queryParams")).length == 0){
                                    "rgs_slot_res"
                            }else if(compact(render(json \ "cmd")).length > 0 &&
                                (compact(render(json \ "cmd")).replace("\"", "").trim == "slotSpun" || compact(render(json \ "cmd")).replace("\"", "").trim == "bonusGameExited" ) &&
                                compact(render(json \ "tf")).length > 0 && compact(render(json \ "tf")).replace("\"", "").trim == "false"){
                                    "non_rgs_slot"
                            }else if (compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "play" &&
                                compact(render(json \ "tf")).length > 0 && compact(render(json \ "tf")).replace("\"", "").trim == "false" &&
                                compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim != "SlotGame" &&
                                compact(render(json \ "outgoing")).length > 0 && compact(render(json \ "outgoing")).replace("\"", "").trim == "false" &&
                                (compact(render(json \ "g")).replace("\"", "").trim == "TableGame-BlackJack" ||
                                compact(render(json \ "g")).replace("\"", "").trim == "VideoPoker" || compact(render(json \ "g")).replace("\"", "").trim == "TableGame-Roulette")){
                                    "rgs_game_req"
                            }else if (compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "play" &&
                                compact(render(json \ "tf")).length > 0 && compact(render(json \ "tf")).replace("\"", "").trim == "false" &&
                                compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim != "SlotGame" &&
                                compact(render(json \ "outgoing")).length > 0 && compact(render(json \ "outgoing")).replace("\"", "").trim == "true" &&
                                (compact(render(json \ "g")).replace("\"", "").trim == "TableGame-BlackJack" ||
                                compact(render(json \ "g")).replace("\"", "").trim == "VideoPoker" || compact(render(json \ "g")).replace("\"", "").trim == "TableGame-Roulette")){
                                    "rgs_game_res"
                            }else if(compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim == "BINGO" &&
                                compact(render(json \ "cmd")).length > 0 &&
                                (compact(render(json \ "cmd")).replace("\"", "").trim == "wonBingo" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "wonConsolationPrize" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "wonJackpot" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "wonHotSpot" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "endRoundBadges" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "gameData" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "openForNextRound" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "newRound" ||
                                    compact(render(json \ "cmd")).replace("\"", "").trim == "playerData")){
                                    "bingo"
                            }else if(compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "endRoundBadges" &&
                                (compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim == "POKER" ||
                                compact(render(json \ "g")).length > 0 && compact(render(json \ "g")).replace("\"", "").trim == "WILD_ACTION_POKER")){
                                    "poker"
                            }else if (compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "initSuccess"){
                                    "init_success"
                            }else if(compact(render(json \ "cmd")).length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "clinfo"){
                                    "cli_info"
                            }else if (compact(render(json \ "cmd")).replace("\"", "").trim.length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "acceptGift"){
                                    "accepted_gifts"
                            }else if (compact(render(json \ "cmd")).replace("\"", "").trim.length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "giftWheelResult"){
                                    "gift_activity"
                            }else if (compact(render(json \ "cmd")).replace("\"", "").trim.length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "endRound" &&
                                compact(render(json \ "g")).replace("\"", "").trim.length > 0 && (compact(render(json \ "g")).replace("\"", "").trim == "POKER" ||
                                compact(render(json \ "g")).replace("\"", "").trim == "WILD_ACTION_POKER")){
                                    "poker_financials"
                           }else  if (compact(render(json \ "cmd")).replace("\"", "").trim.length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "notification" &&
                                compact(render(json \"envelope"\"Type")).replace("\"", "").trim.length > 0 && compact(render(json \"envelope"\"Type")).replace("\"", "").trim == "playerEngine"){
                                    "mpe_ntn"
                            }else if(compact(render(json \ "cmd")).replace("\"", "").trim.length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "notificationAck" &&
                                compact(render(json \"receiver")).replace("\"", "").trim.length > 0 && compact(render(json \"receiver")).replace("\"", "").trim == "playerEngine"){
                                    "mpe_ack"
                            }else if (compact(render(json \ "cmd")).replace("\"", "").trim.length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "metagameAckRequest"){
                                    "meta_game_req"
                            }else if (compact(render(json \ "cmd")).replace("\"", "").trim.length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "metagameAckResponse"){
                                    "meta_game_res"
                            }else if(compact(render(json \ "cmd")).replace("\"", "").replace("\"", "").trim.length > 0 && compact(render(json \ "cmd")).replace("\"", "").trim == "xptInfo" &&
                                compact(render(json \"xptInfo")).replace("\"", "").replace("\"", "").trim.length > 0 && compact(render(json \"userId")).replace("\"", "").replace("\"", "").trim.length > 0 &&
                                compact(render(json \"userId")).replace("\"", "").replace("\"", "").trim != "null"){
                                    "ab_testing"
                            }else{
                                    "other"
                            }
                            val this_records =
                            tbl_name match{
                                case "rgs_slot_req" =>
                                    {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                    }else{null}
                                    //Regex Pattern for transaction ID - To extract anything between the tag <TransactionId></TransactionId>
                                    //Extract transaction ID from XML Body
                                    val txnid = extractFirstMatchingRegex("<TransactionId>(.*)</TransactionId>".r, (json \ "requestBody").extract[String].trim)
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0  ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract slot variation - It is the value before underscore in game variation
                                    val slotstype = gamevariation.split("_")(0)
                                    //REGEX for extracting denomination amount - anything in between zero or more spaces after = and &
                                    //Extracted value can have a negative sign (optional), denomination(mandatory), two decimal points(optional)
                                    //Exponential-E Sign followed by either +/- ve sign and this can be followed by any number of integers [Entire exponential part is optional]
                                    //Extract Base game cost
                                    var temp1 = extractFirstMatchingRegex("&denomamount=\\s*(-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)&".r, (json \ "queryParams").extract[String].trim)
                                    val basegamecost =
                                        if (temp1 != "" && temp1.length >0){
                                            temp1.toDouble
                                        }else{0.toDouble}
                                    //REGEX for extracting number of lines played. Same as denomination amount
                                    //Extract Number of lines played
                                    temp1 = extractFirstMatchingRegex("<PatternsBet>\\s*(-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</PatternsBet>".r, (json \ "requestBody").extract[String].trim)
                                    val nooflinesplayed =
                                        if (temp1 != "" && temp1.length >0){
                                            temp1.toDouble
                                        }else{0.toDouble}
                                    //Pattern to extract bets per pattern
                                    //Extract Bets Per Pattern
                                    temp1 = extractFirstMatchingRegex("<BetPerPattern>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</BetPerPattern>".r,(json \ "requestBody").extract[String].trim)
                                    val betsPerPattern =
                                        if (temp1 != "" && temp1.length >0){
                                            temp1.toDouble
                                        }else{0.toDouble}
                                    // Line Cost = Base Game Cost * Bets Per Pattern
                                    val linecost = basegamecost*betsPerPattern
                                    // Total Bet Amount = Number of Lines Played * Lines Cost
                                    val totalbetamount = nooflinesplayed*linecost
                                    //Extract cash
                                    val cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).replace("\"", "").trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}

                                    gs+","+txnid+","+sessionid+","+userid+","+gamevariation+","+slotstype+","+
                                    basegamecost.toString+","+nooflinesplayed.toString+","+linecost.toString+","+
                                    totalbetamount.toString+","+cash.toString+","+level.toString+","+coins.toString+","+xp.toString+","+ts.toString+","+phpSid+","+currentCID
                                }
                            case "rgs_slot_res" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                        }else{null}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cash
                                    val cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experiment ID
                                    val experimentid =
                                        if (compact(render(json \ "params" \ "experimentName")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "params" \ "experimentName")).replace("\"", "").trim
                                        }else{null}
                                    //Extract experiment group
                                    val experimentgroup  =
                                        if (compact(render(json \ "params" \ "experimentGroup")).replace("\"", "").trim.length > 0  ){
                                            compact(render(json \ "params" \ "experimentGroup")).replace("\"", "").trim
                                        }else{null}
                                    //Extract transaction id, stage, next stage, game status, balance, settled, pending and payout
                                    val (txnid:String, stage:String, nextstage:String, gamestatus:String, balance:Double, settled:Double, pending:Double, payout:Double) =
                                        if(compact(render(json \"params"\"responseBody")).replace("\"", "").trim.length > 0 &&
                                            compact(render(json \"params"\"responseBody")).replace("\"", "").trim != "null"){
                                            (
                                            if (extractFirstMatchingRegex( "<TransactionId>(.*)</TransactionId>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<TransactionId>(.*)</TransactionId>".r, (json  \"params"\"responseBody").extract[String].trim)
                                            }else{null},
                                            if (extractFirstMatchingRegex( "<Stage>(.*)</Stage>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<Stage>(.*)</Stage>".r, (json  \"params"\"responseBody").extract[String])
                                            }else{null},
                                            if (extractFirstMatchingRegex( "<NextStage>(.*)</NextStage>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<NextStage>(.*)</NextStage>".r, (json  \"params"\"responseBody").extract[String].trim)
                                            }else{null},
                                            if (extractFirstMatchingRegex( "<GameStatus>(.*)</GameStatus>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<GameStatus>(.*)</GameStatus>".r, (json  \"params"\"responseBody").extract[String].trim)
                                            }else{null},
                                            if (extractFirstMatchingRegex( "<Balance>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Balance>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<Balance>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Balance>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                            }else{0.toDouble},
                                            if (extractFirstMatchingRegex( "<Settled>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Settled>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<Settled>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Settled>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                            }else{0.toDouble},
                                            if (extractFirstMatchingRegex( "<Pending>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Pending>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<Pending>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Pending>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                            }else{0.toDouble},
                                            if (extractFirstMatchingRegex( "<Payout>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Payout>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                extractFirstMatchingRegex( "<Payout>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Payout>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                            }else{0.toDouble}
                                            )
                                        }else if(compact(render(json \ "params" \"responseObject"\"OutcomeDetail")).length > 0){
                                            (
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"TransactionId")).replace("\"", "").trim.length > 0 ){
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"TransactionId")).replace("\"", "").trim
                                            }else{null},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Stage")).replace("\"", "").trim.length > 0 ){
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Stage")).replace("\"", "").trim
                                            }else{null},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"NextStage")).replace("\"", "").trim.length > 0 ){
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"NextStage")).replace("\"", "").trim
                                            }else{null},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"GameStatus")).replace("\"", "").trim.length > 0 ){
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"GameStatus")).replace("\"", "").trim
                                            }else{null},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim !="" &&
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim.toDouble
                                            }else{0.toDouble},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim !="" &&
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim.toDouble
                                            }else{0.toDouble},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim !="" &&
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim.toDouble
                                            }else{0.toDouble},
                                            if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim !="" &&
                                                compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim.toDouble
                                            }else{0.toDouble}
                                            )
                                        }else{
                                            ("","","","",0.toDouble,0.toDouble,0.toDouble,0.toDouble)
                                    }

                                    val firstWinningString =
                                        if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winning")).replace("\"", "").trim.length > 0){
                                            compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winning")).replace("\"", "").trim
                                        }else{
                                            null
                                        }

                                    val secondWinningString =
                                        if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winning")).replace("\"", "").trim.length > 0){
                                            compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winning")).replace("\"", "").trim
                                        }else{
                                            null
                                        }

                                    val firstAmount =
                                        if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winAmount")).replace("\"", "").trim.length > 0){
                                            compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winAmount")).replace("\"", "").trim
                                        }else{
                                            "0"
                                        }

                                    val secondAmount =
                                        if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winAmount")).replace("\"", "").trim.length > 0){
                                            compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winAmount")).replace("\"", "").trim
                                        }else{
                                            "0"
                                        }

                                    val firstJackpot =
                                        if ( compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0) \"jackpotID")).replace("\"", "").trim.length > 0){
                                            compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0) \"jackpotID")).replace("\"", "").trim
                                        }else{
                                            null
                                        }

                                    val secondJackpot =
                                        if ( compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1) \"jackpotID")).replace("\"", "").trim.length > 0){
                                            compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1) \"jackpotID")).replace("\"", "").trim
                                        }else{
                                            null
                                        }
                                    val jackpot:Long =
                                        if (firstWinningString == "true" && secondWinningString=="false" && firstAmount!="null"){
                                            firstAmount.toLong
                                        }else if (firstWinningString == "false" && secondWinningString=="true" && secondAmount!="null" ){
                                            secondAmount.toLong
                                        }else if (firstWinningString == "true" && secondWinningString=="true" && firstAmount!="null" && secondAmount!="null"){
                                            firstAmount.toLong+secondAmount.toLong
                                        }else{
                                            0.toLong
                                        }
                                    val jackpotid:String =
                                        if (firstWinningString == "true" && secondWinningString=="false" && firstJackpot!="null"){
                                            firstJackpot
                                        }else if (firstWinningString == "false" && secondWinningString=="true" && secondJackpot!="null"){
                                            secondJackpot
                                        }else if (firstWinningString == "true" && secondWinningString=="true" && firstJackpot!="null" && secondJackpot!="null"){
                                            firstJackpot+"&"+secondJackpot
                                        }else{
                                            null
                                        }
                                    gs+","+txnid+","+sessionid+","+userid+","+stage+","+ nextstage+","+gamestatus+","+balance.toString+","+settled.toString+","+pending.toString+","+payout.toString+","+
                                    cash.toString+","+level.toString+","+coins.toString+","+xp.toString+","+ts.toString+","+jackpot.toLong+","+experimentid+","+experimentgroup+","+jackpotid
                                }
                            case  "non_rgs_slot" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                        }else{null}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract slots type
                                    val slotstype =
                                        if (compact(render(json \"params"\"slotsType")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"slotsType")).replace("\"", "").trim
                                        }else{null}
                                    //Extract command
                                    val cmd =
                                        if (compact(render(json \ "cmd")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "cmd")).replace("\"", "").trim
                                        }else{null}
                                    //Extract line cost
                                    val linecost =
                                        if (compact(render(json \"params"\"lineCost")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json  \"params"\"lineCost")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract spins, linesplayed, lineswon, lineCost:linewinnings, freespinwinnings, winnings
                                    val (spins, linesplayed:Long, lineswon, lineCost:Long, linewinnings:Double, freespinwinnings:Double, winnings:Double) =
                                        if (compact(render(json \"cmd")).replace("\"", "").trim.length > 0 &&
                                            compact(render(json \"cmd")).replace("\"", "").trim == "slotSpun"){
                                                (//Spins
                                                1,
                                                //lines played
                                                if (compact(render(json \"params"\"linesPlayed")).replace("\"", "").trim.length > 0 ){
                                                    compact(render(json  \"params"\"linesPlayed")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                //lines won
                                                if (compact(render(json \"params"\"winLines")).replace("\"", "").trim.length > 0 &&
                                                    (json \"params"\"winLines").extract[List[JValue]].length != 0 ){
                                                        (json \"params"\"winLines").extract[List[JValue]].length
                                                }else{0},
                                                //line cost
                                                if (compact(render(json \"params"\"lineCost")).replace("\"", "").trim.length > 0 ){
                                                    compact(render(json  \"params"\"lineCost")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                //line winnings
                                                if (compact(render(json  \"params"\"winnings")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"winnings")).replace("\"", "").trim.length > 0){
                                                        compact(render(json  \"params"\"winnings")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble},
                                                //free spin winnings
                                                if (compact(render(json  \"params"\"totalFreeSpinWinnings")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"totalFreeSpinWinnings")).replace("\"", "").trim.length > 0){
                                                        compact(render(json  \"params"\"totalFreeSpinWinnings")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble},
                                                //winnings
                                                if (compact(render(json  \"params"\"totalWinnings")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"totalWinnings")).replace("\"", "").trim.length > 0){
                                                        compact(render(json  \"params"\"totalWinnings")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble})
                                            }else{
                                                (0,0.toLong,0,0.toLong,0.toDouble,0.toDouble,0.toDouble)
                                            }
                                    //Extract bet amount
                                    val betamount = linesplayed * lineCost
                                    //Extract bet lost
                                    val betlost = betamount - linewinnings
                                    //Extract jackpot
                                    val jackpot =
                                    if (compact(render(json \"cmd")).replace("\"", "").trim.length > 0 &&
                                        compact(render(json \"cmd")).replace("\"", "").trim == "slotSpun" &&
                                        compact(render(json \"params"\"wonJackpot")).replace("\"", "").trim.length > 0 &&
                                        compact(render(json \"params"\"wonJackpot")).replace("\"", "").trim == "true" &&
                                        compact(render(json \"params"\"jackpot")).replace("\"", "").trim !="" &&
                                        compact(render(json \"params"\"jackpot")).replace("\"", "").trim.length > 0){
                                            compact(render(json \"params"\"jackpot")).replace("\"", "").trim.toDouble
                                    }else{0.toDouble}
                                    //Extract bonus winnings
                                    val bonuswinnings =
                                        if (compact(render(json \"cmd")).replace("\"", "").trim.length > 0 &&
                                            compact(render(json \"cmd")).replace("\"", "").trim == "bonusGameExited" &&
                                            compact(render(json \"params"\"totalWinnings")).replace("\"", "").trim.length > 0 &&
                                            compact(render(json \"params"\"totalWinnings")).replace("\"", "").trim == "true"){
                                                compact(render(json \"params"\"totalWinnings")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract end cash
                                    val endcash =
                                        if (compact(render(json  \"params"\"userData"\"cash")).replace("\"", "").trim !="" &&
                                            compact(render(json  \"params"\"userData"\"cash")).replace("\"", "").trim.length > 0){
                                                compact(render(json  \"params"\"userData"\"cash")).replace("\"", "").trim.toDouble
                                        }else{0.toDouble}
                                    //Extract begining cash
                                    val begincash = endcash + (betamount - (if(linewinnings>0) linewinnings else winnings))
                                    //Extract level
                                    val level =
                                        if (compact(render(json \"params"\"userData"\"level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"userData"\"level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \"params"\"userData"\"coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"userData"\"coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract rockets
                                    val rockets =
                                        if (compact(render(json  \"params"\"rockets")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json  \"params"\"rockets")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract xp
                                    val xp =
                                        if (compact(render(json  \"params"\"userData"\"xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json  \"params"\"userData"\"xp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract ts
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    //Return required fields
                                    gs+","+sessionid+","+userid+","+ gamevariation+","+ slotstype+","+ cmd+","+linecost.toString+","+spins.toString+","+linesplayed.toString+","+
                                    lineswon.toString+","+ betamount.toString+","+ betlost.toString+","+ bonuswinnings.toString+","+jackpot.toString+","+freespinwinnings.toString+","+
                                    linewinnings.toString+","+winnings.toString+","+begincash.toString+","+endcash.toString+","+level.toString+","+coins.toString+","+
                                    rockets.toString+","+xp.toString+","+ts.toString+","+phpSid+","+currentCID
                                }
                            case "rgs_game_req" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                    }else{null}
                                    //Regex Pattern for transaction ID - To extract anything between the tag <TransactionId></TransactionId>
                                    //Extract transaction ID from XML Body
                                    val txnid = extractFirstMatchingRegex("<TransactionId>(.*)</TransactionId>".r, compact(render(json \ "requestBody")).trim)
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game type
                                    val gametype =
                                        if (compact(render(json \ "g")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "g")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract base line cost
                                    val baselinecost =
                                        if (extractFirstMatchingRegex("&denomamount=\\s*(-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)&".r, compact(render(json \ "queryParams")).trim).length > 0 ){
                                            extractFirstMatchingRegex("&denomamount=\\s*(-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)&".r,(json \ "queryParams").extract[String].trim).toDouble
                                        }else{0.toDouble}
                                    //Extract cash
                                    val cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                    }else{"0"}
                                    gs+","+txnid+","+sessionid+","+userid+","+gametype+","+gamevariation+","+baselinecost.toString+","+cash.toString+","+level.toString+","+
                                    coins.toString+","+xp.toString+","+ts.toString+","+phpSid+","+currentCID
                                }
                            case "rgs_game_res" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                        }else{null}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cash
                                    val cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experiment ID
                                    val experimentid =
                                        if (compact(render(json \ "params" \ "experimentName")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "params" \ "experimentName")).replace("\"", "").trim
                                        }else{null}
                                    //Extract experiment group
                                    val experimentgroup  =
                                        if (compact(render(json \ "params" \ "experimentGroup")).replace("\"", "").trim.length > 0  ){
                                            compact(render(json \ "params" \ "experimentGroup")).replace("\"", "").trim
                                        }else{null}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                               compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    //Extract transaction id, stage, next stage, game status, balance, settled, pending and payout
                                    val (txnid:String, stage:String, nextstage:String, gamestatus:String, balance:Double, settled:Double, pending:Double, payout:Double) =
                                        if(compact(render(json \"params"\"responseBody")).replace("\"", "").trim.length > 0 &&
                                            compact(render(json \"params"\"responseBody")).replace("\"", "").trim != "null"){
                                            (
                                                if (extractFirstMatchingRegex( "<TransactionId>(.*)</TransactionId>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                    extractFirstMatchingRegex( "<TransactionId>(.*)</TransactionId>".r, (json  \"params"\"responseBody").extract[String].trim)
                                                }else{null},
                                                if (extractFirstMatchingRegex( "<Stage>(.*)</Stage>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                    extractFirstMatchingRegex( "<Stage>(.*)</Stage>".r, (json  \"params"\"responseBody").extract[String])
                                                }else{null},
                                                if (extractFirstMatchingRegex( "<NextStage>(.*)</NextStage>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                    extractFirstMatchingRegex( "<NextStage>(.*)</NextStage>".r, (json  \"params"\"responseBody").extract[String].trim)
                                                }else{null},
                                                if (extractFirstMatchingRegex( "<GameStatus>(.*)</GameStatus>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                    extractFirstMatchingRegex( "<GameStatus>(.*)</GameStatus>".r, (json  \"params"\"responseBody").extract[String].trim)
                                                }else{null},
                                                if (extractFirstMatchingRegex( "<Balance>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Balance>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                        extractFirstMatchingRegex( "<Balance>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Balance>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                                }else{0.toDouble},
                                                if (extractFirstMatchingRegex( "<Settled>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Settled>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                        extractFirstMatchingRegex( "<Settled>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Settled>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                                }else{0.toDouble},
                                                if (extractFirstMatchingRegex( "<Pending>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Pending>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                        extractFirstMatchingRegex( "<Pending>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Pending>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                                }else{0.toDouble},
                                                if (extractFirstMatchingRegex( "<Payout>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Payout>".r, (json  \"params"\"responseBody").extract[String]).length > 0 ){
                                                        extractFirstMatchingRegex( "<Payout>(\\s*-?\\d+(\\.\\d+)?(E[\\+-]?\\d+)?\\s*)</Payout>".r, (json  \"params"\"responseBody").extract[String].trim).toDouble
                                                }else{0.toDouble}
                                            )
                                        }else if(compact(render(json \ "params" \"responseObject"\"OutcomeDetail")).length > 0){
                                            (
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"TransactionId")).replace("\"", "").trim.length > 0 ){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"TransactionId")).replace("\"", "").trim
                                                }else{null},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Stage")).replace("\"", "").trim.length > 0 ){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Stage")).replace("\"", "").trim
                                                }else{null},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"NextStage")).replace("\"", "").trim.length > 0 ){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"NextStage")).replace("\"", "").trim
                                                }else{null},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"GameStatus")).replace("\"", "").trim.length > 0 ){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"GameStatus")).replace("\"", "").trim
                                                }else{null},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Balance")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Settled")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Pending")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble},
                                                if (compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim !="" &&
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim.length > 0){
                                                    compact(render(json  \"params"\"responseObject"\"OutcomeDetail"\"Payout")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble}
                                            )
                                        }else{
                                            ("","","","",0.toDouble,0.toDouble,0.toDouble,0.toDouble)
                                        }

                                        val firstWinningString =
                                            if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winning")).replace("\"", "").trim.length > 0){
                                                compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winning")).replace("\"", "").trim
                                            }else{
                                                null
                                            }

                                        val secondWinningString =
                                            if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winning")).replace("\"", "").trim.length > 0){
                                                compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winning")).replace("\"", "").trim
                                            }else{
                                                null
                                            }

                                        val firstAmount =
                                            if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winAmount")).replace("\"", "").trim.length > 0){
                                                compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winAmount")).replace("\"", "").trim
                                            }else{
                                                "0"
                                            }

                                        val secondAmount =
                                            if (compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winAmount")).replace("\"", "").trim.length > 0){
                                                compact(render((json  \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winAmount")).replace("\"", "").trim
                                            }else{
                                                "0"
                                            }

                                        val firstJackpot =
                                            if ( compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0) \"jackpotID")).replace("\"", "").trim.length > 0){
                                                compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0) \"jackpotID")).replace("\"", "").trim
                                            }else{
                                                null
                                            }

                                        val secondJackpot =
                                            if ( compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1) \"jackpotID")).replace("\"", "").trim.length > 0){
                                                compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1) \"jackpotID")).replace("\"", "").trim
                                            }else{
                                                null
                                            }
                                        val jackpot:Long =
                                            if (firstWinningString == "true" && secondWinningString=="false" && firstAmount!="null"){
                                                firstAmount.toLong
                                            }else if (firstWinningString == "false" && secondWinningString=="true" && secondAmount!="null" ){
                                                secondAmount.toLong
                                            }else if (firstWinningString == "true" && secondWinningString=="true" && firstAmount!="null" && secondAmount!="null"){
                                                firstAmount.toLong+secondAmount.toLong
                                            }else{
                                                0.toLong
                                            }

                                        val jackpotid:String =
                                            if (firstWinningString == "true" && secondWinningString=="false" && firstJackpot!="null"){
                                                firstJackpot
                                            }else if (firstWinningString == "false" && secondWinningString=="true" && secondJackpot!="null"){
                                                secondJackpot
                                            }else if (firstWinningString == "true" && secondWinningString=="true" && firstJackpot!="null" && secondJackpot!="null"){
                                                firstJackpot+"&"+secondJackpot
                                            }else{
                                                null
                                            }

                                        val (experimentGroup_rgsSlotJackpot:String, experimentName_rgsSlotJackpot:String, gameId_rgsSlotJackpot:String) =
                                            if (firstWinningString == "true" || secondWinningString=="true"){
                                                (
                                                    if (compact(render(json \"params"\"jackpotWinningInfo"\"experimentGroup")).replace("\"", "").trim.length > 0){
                                                        compact(render(json \"params"\"jackpotWinningInfo"\"experimentGroup")).replace("\"", "").trim
                                                    }else{
                                                        ""
                                                    },
                                                    if (compact(render(json \"params"\"jackpotWinningInfo"\"experimentName")).replace("\"", "").trim.length > 0){
                                                        compact(render(json \"params"\"jackpotWinningInfo"\"experimentName")).replace("\"", "").trim
                                                    }else{
                                                        ""
                                                    },
                                                    if (compact(render(json \"params"\"jackpotWinningInfo"\"gameId")).replace("\"", "").trim.length > 0){
                                                        compact(render(json \"params"\"jackpotWinningInfo"\"gameId")).replace("\"", "").trim
                                                    }else{
                                                        ""
                                                    }
                                                )
                                            }else{
                                                ("","","")
                                            }
                                        val prefix = gs+","+txnid+","+sessionid+","+userid+","+stage+","+ nextstage+","+gamestatus+","+balance.toString+","+settled.toString+","+pending.toString+","+
                                        payout.toString+","+cash.toString+","+level.toString+","+coins.toString+","+xp.toString+","+ts.toString+","+jackpot.toLong+","+experimentid+","+experimentgroup+","+
                                        jackpotid+","+phpSid+","+currentCID+","+experimentGroup_rgsSlotJackpot+","+experimentName_rgsSlotJackpot+","+gameId_rgsSlotJackpot+","
                                        var finalRec = ""
                                        //Extract RGS Slot Jackpot Info
                                        if (firstWinningString == "true"){
                                            //Extract growth per second
                                            val growthPerSecond_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"growthPerSecond")).replace("\"", "").trim !="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"growthPerSecond")).replace("\"", "").trim.length > 0 &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"growthPerSecond")).replace("\"", "").trim!="null" ){
                                                        compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"growthPerSecond")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble}
                                            //Extract hot
                                            val hot_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"hot")).replace("\"", "").trim.length > 0 ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"hot")).replace("\"", "").trim
                                                }else{null}
                                            //Extract jackpot id
                                            val jackpotID_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"jackpotID")).replace("\"", "").trim.length > 0 ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"jackpotID")).replace("\"", "").trim
                                                }else{null}
                                            //Extract jackpot instance id
                                            val jackpotInstanceId_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"jackpotInstanceId")).replace("\"", "").trim.length > 0 ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"jackpotInstanceId")).replace("\"", "").trim
                                                }else{null}
                                            //Extract minimum bet amount
                                            val minBetAmount_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"minBetAmount")).replace("\"", "").trim.length > 0 &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"minBetAmount")).replace("\"", "").trim!="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"minBetAmount")).replace("\"", "").trim!="null" ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"minBetAmount")).replace("\"", "").trim.toLong
                                                }else{0.toLong}
                                            //Extract new value
                                            val newValue_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"newValue")).replace("\"", "").trim.length > 0 &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"newValue")).replace("\"", "").trim!="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"newValue")).replace("\"", "").trim!="null"){
                                                        compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"newValue")).replace("\"", "").trim.toLong
                                                }else{0.toLong}
                                            //Extract skin id
                                            val skinID_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"skinID")).replace("\"", "").trim.length > 0 ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"skinID")).replace("\"", "").trim
                                                }else{null}
                                            //Extract unique jackpot id
                                            val uniqueJackpotID_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"uniqueJackpotID")).replace("\"", "").trim.length > 0 ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"uniqueJackpotID")).replace("\"", "").trim
                                                }else{null}
                                            //Extract value
                                            val value_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"value")).replace("\"", "").trim.length > 0  &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"value")).replace("\"", "").trim!="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"value")).replace("\"", "").trim!="null"){
                                                        compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"value")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble}
                                            //Extract win amount
                                            val winAmount_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winAmount")).replace("\"", "").trim.length > 0 &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winAmount")).replace("\"", "").trim!="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winAmount")).replace("\"", "").trim!="null"){
                                                        compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winAmount")).replace("\"", "").trim.toLong
                                                }else{0.toLong}
                                            //Extract winning
                                            val winning_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winning")).replace("\"", "").trim.length > 0  &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winning")).replace("\"", "").trim!="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winning")).replace("\"", "").trim!="null"){
                                                        compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winning")).replace("\"", "").trim
                                                }else{null}

                                            finalRec+=prefix+growthPerSecond_rgsSlotJackpot.toString+","+hot_rgsSlotJackpot+","+jackpotID_rgsSlotJackpot+","+
                                            jackpotInstanceId_rgsSlotJackpot+","+minBetAmount_rgsSlotJackpot.toString+","+newValue_rgsSlotJackpot.toString+","+skinID_rgsSlotJackpot+","+
                                            uniqueJackpotID_rgsSlotJackpot+","+value_rgsSlotJackpot.toString+","+winAmount_rgsSlotJackpot.toString+","+winning_rgsSlotJackpot.toString+"\t"
                                        }else if (secondWinningString == "true"){
                                            //Extract growth per second
                                            val growthPerSecond_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"growthPerSecond")).replace("\"", "").trim !="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"growthPerSecond")).replace("\"", "").trim.length > 0 &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"growthPerSecond")).replace("\"", "").trim!="null" ){
                                                        compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"growthPerSecond")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble}
                                            //Extract hot
                                            val hot_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"hot")).replace("\"", "").trim.length > 0 ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"hot")).replace("\"", "").trim
                                                }else{null}
                                            //Extract jackpot id
                                            val jackpotID_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"jackpotID")).replace("\"", "").trim.length > 0 ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"jackpotID")).replace("\"", "").trim
                                                }else{null}
                                            //Extract jackpot instance id
                                            val jackpotInstanceId_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"jackpotInstanceId")).replace("\"", "").trim.length > 0 ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"jackpotInstanceId")).replace("\"", "").trim
                                                }else{null}
                                            //Extract minimum bet amount
                                            val minBetAmount_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"minBetAmount")).replace("\"", "").trim.length > 0 &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"minBetAmount")).replace("\"", "").trim!="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"minBetAmount")).replace("\"", "").trim!="null" ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"minBetAmount")).replace("\"", "").trim.toLong
                                                }else{0.toLong}
                                            //Extract new value
                                            val newValue_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"newValue")).replace("\"", "").trim.length > 0 &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"newValue")).replace("\"", "").trim!="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"newValue")).replace("\"", "").trim!="null"){
                                                        compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"newValue")).replace("\"", "").trim.toLong
                                                }else{0.toLong}
                                            //Extract skin id
                                            val skinID_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"skinID")).replace("\"", "").trim.length > 0 ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"skinID")).replace("\"", "").trim
                                                }else{null}
                                            //Extract unique jackpot id
                                            val uniqueJackpotID_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"uniqueJackpotID")).replace("\"", "").trim.length > 0 ){
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"uniqueJackpotID")).replace("\"", "").trim
                                                }else{null}
                                            //Extract value
                                            val value_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"value")).replace("\"", "").trim.length > 0  &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"value")).replace("\"", "").trim!="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"value")).replace("\"", "").trim!="null"){
                                                        compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"value")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble}
                                            //Extract win amount
                                            val winAmount_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winAmount")).replace("\"", "").trim.length > 0 &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winAmount")).replace("\"", "").trim!="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winAmount")).replace("\"", "").trim!="null"){
                                                        compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winAmount")).replace("\"", "").trim.toLong
                                                }else{0.toLong}
                                            //Extract winning
                                            val winning_rgsSlotJackpot =
                                                if (compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winning")).replace("\"", "").trim.length > 0  &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winning")).replace("\"", "").trim!="" &&
                                                    compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(1)\"winning")).replace("\"", "").trim!="null"){
                                                        compact(render((json \"params"\"jackpotWinningInfo"\"jackpotWinningInfoList")(0)\"winning")).replace("\"", "").trim
                                                }else{null}

                                            finalRec+=prefix+growthPerSecond_rgsSlotJackpot.toString+","+hot_rgsSlotJackpot+","+jackpotID_rgsSlotJackpot+","+
                                            jackpotInstanceId_rgsSlotJackpot+","+minBetAmount_rgsSlotJackpot.toString+","+newValue_rgsSlotJackpot.toString+","+
                                            skinID_rgsSlotJackpot+","+uniqueJackpotID_rgsSlotJackpot+","+value_rgsSlotJackpot.toString+","+winAmount_rgsSlotJackpot.toString+","+winning_rgsSlotJackpot.toString+"\t"
                                        }else{
                                            finalRec+=prefix+0.toString+",,,,"+0.toString+","+0.toString+",,,"+0.toString+","+0.toString+","+0.toString+"\t"
                                        }
                                        finalRec.trim
                                }
                            case "bingo" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                        }else{null}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game type
                                    val gametype =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (gametype != null && gametype!= "null" && gametype.length>0){
                                            gametype.trim.substring(0, gametype.trim.lastIndexOf(" "))
                                        }else{null}
                                    //Extract play type
                                    val playtype =
                                        if (gametype != null && gametype!= "null" && gametype.length>0){
                                            gametype.trim.substring(gametype.trim.indexOf("_")+1, gametype.trim.lastIndexOf("_"))
                                        }else{null}
                                    //Extract round id
                                    val roundid =
                                        if (compact(render(json \ "roundId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "roundId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cmd
                                    val cmd =
                                        if (compact(render(json \ "cmd")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "cmd")).replace("\"", "").trim
                                        }else{null}
                                    //Extract number of cards played
                                    val noofcardsplayed =
                                        if (compact(render(json \"params"\"purchasedCardCount")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"purchasedCardCount")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract card cost
                                    val cardcost =
                                        if (compact(render(json \"params"\"cardcost")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"cardcost")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract bingo winnings
                                    val bingowinnings =
                                        if (cmd =="wonBingo" && compact(render(json \ "params"\"winnings")).replace("\"", "").trim.length > 0){
                                            compact(render(json \ "params"\"winnings")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract hot chip winnings
                                    val hotchipwinnings =
                                        if (cmd =="wonHotSpot" && compact(render(json \ "params"\"amount")).replace("\"", "").trim.length > 0){
                                            compact(render(json \ "params"\"amount")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract consolation winnings
                                    val consolationwinnings =
                                        if (cmd =="wonConsolationPrize" && compact(render(json \ "params"\"amount")).replace("\"", "").trim.length > 0){
                                            compact(render(json \ "params"\"amount")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract jackpot winnings
                                    val jackpotwinnings =
                                        if (cmd =="wonJackpot" && compact(render(json \ "params"\"winnings")).replace("\"", "").trim.length > 0){
                                            compact(render(json \ "params"\"winnings")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract is quit game
                                    val isquitgame =
                                        if (cmd =="endRoundBadges"){
                                            "false"
                                        }else{null}
                                    //Exract autodaub
                                    val autodaub =
                                        if (cmd == "gameData" &&
                                            compact(render(json \ "params"\"autoPilot")).replace("\"", "").trim.length > 0){
                                                compact(render(json \"params"\"autoPilot")).replace("\"", "").trim
                                        }else if (cmd =="newRound" &&
                                                compact(render(json \ "params"\"autoDaub")).replace("\"", "").trim.length > 0){
                                                    compact(render(json \ "params"\"autoDaub")).replace("\"", "").trim
                                        }else{null}
                                    //Extract pattern
                                    val pattern_str =
                                        if ((cmd  == "openForNextRound" ||
                                            cmd == "newRound") &&
                                            compact(render(json \"params"\"pattern")).replace("\"", "").trim.length > 0){
                                                compact(render(json \"params"\"pattern")).replace("\"", "").trim
                                        }else{null}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    gs+","+sessionid+","+userid+","+gamevariation+","+gametype+","+playtype+","+roundid+","+cmd+","+noofcardsplayed.toString+","+
                                    cardcost.toString+","+bingowinnings.toString+","+hotchipwinnings.toString+","+consolationwinnings.toString+","+jackpotwinnings.toString+","+
                                    isquitgame+","+autodaub+","+pattern_str+","+ts.toString+","+phpSid+","+currentCID
                                }
                            case "poker" =>
                                {
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                        }else{null}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game type
                                    val gametype =
                                        if (compact(render(json \ "g")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "g")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract total bet amount
                                    val totalbetamount =
                                        if (compact(render(json \"params"\"betInRound")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"betInRound")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract total amount won
                                    val totalamountwon =
                                        if (compact(render(json \"params"\"winInRound")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"winInRound")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract total amount lost
                                    val totalamountlost = totalbetamount - totalamountwon
                                    //Set Number of rounds
                                    val noofrounds=1
                                    //Set number of rounds won
                                    val noofroundswon = if(totalamountwon>0) 1 else 0
                                    //Extract end cash
                                    val end_cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract beg cash
                                    val beg_cash = end_cash + totalbetamount - totalamountwon
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    gs+","+sessionid+","+userid+","+gametype+","+gamevariation+","+totalbetamount.toString+","+totalamountwon.toString+","+totalamountlost.toString+","+
                                    noofrounds.toString+","+noofroundswon.toString+","+beg_cash.toString+","+end_cash.toString+","+level.toString+","+coins.toString+","+ts.toString+","+
                                    phpSid+","+currentCID
                                }
                            case "init_success" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    //Extract blue box flag
                                    val bluebox_flag =
                                        if (compact(render(json \ "b")).trim.length > 0 ){
                                            compact(render(json \ "b")).replace("\"", "").trim
                                        }else{null}
                                    //Extract psc, friend_bonus, spin_bonus, day_bonus, total_bonus, wheelstoplocation and sfs_sessionid
                                    val (psc:Double, friend_bonus:Long, spin_bonus:Long, day_bonus:Long, total_bonus:Long, wheelstoplocation:Long, sfs_sessionid:Long) =
                                        if (compact(render(json \ "params")).replace("\"", "").trim.length > 0 && compact(render(json \ "params")).replace("\"", "").trim!="null"){
                                            (
                                                if(compact(render(json \"params"\"psc")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"psc")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"psc")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble},
                                                if(compact(render(json \"params"\"numFriendsBonus")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"numFriendsBonus")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"numFriendsBonus")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"spinBonus")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"spinBonus")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"spinBonus")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"consecutiveDaysBonus")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"consecutiveDaysBonus")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"consecutiveDaysBonus")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"totalBonus")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"totalBonus")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"totalBonus")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"wheelStopLocation")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"wheelStopLocation")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"wheelStopLocation")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"sessionId")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"sessionId")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"sessionId")).replace("\"", "").trim.toLong
                                                }else{0.toLong}
                                                )
                                        }else{
                                            (0.toDouble,0.toLong,0.toLong,0.toLong,0.toLong,0.toLong,0.toLong)
                                        }
                                    //Extract cash, coins, level
                                    val (cash:Double, coins:Long, level:Long) =
                                        if (compact(render(json \ "params"\"userData")).replace("\"", "").trim.length > 0 && compact(render(json \ "params"\"userData")).replace("\"", "").trim!="null"){
                                            (
                                                if(compact(render(json\"params"\"userData"\"cash")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"userData"\"cash")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"userData"\"cash")).replace("\"", "").trim.toDouble
                                                }else{0.toDouble},
                                                if(compact(render(json \"params"\"userData"\"coins")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"userData"\"coins")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"userData"\"coins")).replace("\"", "").trim.toLong
                                                }else{0.toLong},
                                                if(compact(render(json \"params"\"userData"\"level")).replace("\"", "").trim.length > 0 &&
                                                    compact(render(json \"params"\"userData"\"level")).replace("\"", "").trim !=""){
                                                        compact(render(json \"params"\"userData"\"level")).replace("\"", "").trim.toLong
                                                }else{0.toLong}
                                            )
                                         }else{
                                             (0.toDouble,0.toLong,0.toLong)
                                        }
                                        sessionid+","+userid+","+psc.toString+","+cash.toString+","+coins.toString+","+
                                        level.toString+","+friend_bonus.toString+","+spin_bonus.toString+","+day_bonus.toString+","+
                                        total_bonus.toString+","+wheelstoplocation.toString+","+bluebox_flag+","+sfs_sessionid.toString+","+ts.toString+","+phpSid+","+currentCID
                                }

                            case "cli_info" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract os
                                    val os =
                                        if (compact(render(json \ "os")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "os")).replace("\"", "").trim
                                        }else{null}
                                    //Extract device
                                    val device =
                                        if (compact(render(json \ "device")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "device")).replace("\"", "").trim
                                        }else{null}
                                    //Extract client type
                                    val cltype =
                                        if (compact(render(json \ "cltype")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "cltype")).replace("\"", "").trim
                                        }else{null}
                                    //Extract facebook id
                                    val fbid =
                                        if (compact(render(json \ "fbid")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "fbid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract device id
                                    val device_id =
                                        if (compact(render(json \ "device_id")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "device_id")).replace("\"", "").trim
                                        }else{null}
                                    //Extract language
                                    val lang =
                                        if (compact(render(json \ "lang")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "lang")).replace("\"", "").trim
                                        }else{null}
                                    //Extract browser
                                    val browser =
                                        if (compact(render(json \ "browser")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "browser")).trim
                                        }else{null}
                                    sessionid +","+ os  +","+ device +","+ cltype +","+ fbid +","+ device_id +","+ lang +","+ browser +","+ ts.toString
                                }
                            case "accepted_gifts" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract gift id
                                    val gift_id =
                                        if (compact(render(json \ "giftId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "giftId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract recipient user id
                                    val recipient_user_id =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                        gift_id+","+recipient_user_id+","+ts.toString
                                }
                            case "gift_activity" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                            compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    //Extract cash
                                    val cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract wheel index
                                    val wheelindex =
                                        if (compact(render(json \ "wheelIndex")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "wheelIndex")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    userid+","+sessionid+","+cash.toString+","+level.toString+","+coins.toString+","+
                                    wheelindex.toString+","+xp.toString+","+ts.toString+","+phpSid+","+currentCID
                                }
                            case "poker_financials" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                           compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    //Extract cash
                                    val cash =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                       }else{0.toLong}
                                    //Extract game type
                                    val game =
                                        if (compact(render(json \ "g")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "g")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract round id
                                    val roundid =
                                        if (compact(render(json \"params"\"roundId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params"\"roundId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract win total
                                    val wintotal =
                                        if (compact(render(json \"params" \"financials"\"winTotal")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params" \"financials"\"winTotal")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract bet total
                                    val bettotal =
                                        if (compact(render(json \"params" \"financials"\"betTotal")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params" \"financials"\"betTotal")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract rake total
                                    val raketotal =
                                        if (compact(render(json \"params" \"financials"\"rakeTotal")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \"params" \"financials"\"rakeTotal")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    sessionid+","+userid+","+game+","+gamevariation+","+level.toString+","+xp.toString+","+
                                    cash.toString+","+coins.toString+","+roundid+","+wintotal.toString+","+bettotal.toString+","+
                                    raketotal.toString+","+ts.toString+","+phpSid+","+currentCID
                                }
                            case "mpe_ntn" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                           compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    //Extract notification id
                                    val notificationid =
                                        if (compact(render(json \ "envelope"\"NotificationId")).trim.length > 0 ){
                                            compact(render(json  \ "envelope"\"NotificationId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game type
                                    val gametype =
                                        if (compact(render(json \ "g")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "g")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gamevariation =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract chips
                                    val chips =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                       }else{0.toLong}
                                    //Extract experiment
                                    val experiment =
                                        if (compact(render(json \ "envelope"\"Experiment")).trim.length > 0 ){
                                            compact(render(json \ "envelope"\"Experiment")).replace("\"", "").trim
                                        }else{null}
                                    //Extract Promo ID
                                    val promoid =
                                        if (compact(render((json \"body"\"promotions")(0) \"promotionId")).trim.length > 0 ){
                                            compact(render((json \"body"\"promotions")(0) \"promotionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract chips rewards
                                    val chips_reward =
                                        if (compact(render((json \"body"\"promotions")(0) \"behaviorConfig"\"params"\"chipsReward")).trim.length > 0 ){
                                            compact(render((json \"body"\"promotions")(0) \"behaviorConfig"\"params"\"chipsReward")).replace("\"", "").trim
                                        }else{null}
                                    //Extract award status
                                    val award_status =
                                        if (compact(render((json \"body"\"promotions")(0) \"behaviorConfig"\"params"\"awarding")).trim.length > 0 ){
                                            compact(render((json \"body"\"promotions")(0) \"behaviorConfig"\"params"\"awarding")).replace("\"", "").trim
                                        }else{null}

                                    //Extract value 1
                                    val val1 =
                                        if( compact(render(((json \"body"\"promotions")(0) \"modelConfig" \"rewards")(0)\"type")).replace("\"", "").trim.length > 0){
                                            compact(render(((json \"body"\"promotions")(0) \"modelConfig" \"rewards")(0)\"type")).replace("\"", "").trim
                                        }else{null}
                                    //Extract value 2
                                    val val2 =
                                        if( compact(render(((json \"body"\"promotions")(0) \"modelConfig" \"rewards")(0)\"rewardType")).replace("\"", "").trim.length > 0){
                                            compact(render(((json \"body"\"promotions")(0) \"modelConfig" \"rewards")(0)\"rewardType")).replace("\"", "").trim
                                        }else{null}
                                    //Extract reward
                                    val reward =
                                        if(val1 =="chips" || val2 =="chips"){
                                            if( compact(render(((json \"body"\"promotions")(0) \"modelConfig" \"rewards")(0)\"params"\"chips")).replace("\"", "").trim.length > 0){
                                                compact(render(((json \"body"\"promotions")(0) \"modelConfig" \"rewards")(0)\"params"\"chips")).replace("\"", "").trim
                                            }else{null}
                                        }else{
                                            if( compact(render(((json \"body"\"promotions")(0) \"modelConfig" \"rewards")(0)\"reward")).replace("\"", "").trim.length > 0){
                                                compact(render(((json \"body"\"promotions")(0) \"modelConfig" \"rewards")(0)\"reward")).replace("\"", "").trim
                                            }else{null}
                                        }
                                    //Extract reward type
                                    val rewardType =
                                        if (val1 != "null" && val1!=null){
                                            val1
                                        }else if(val2 !="null" && val2!=null){
                                            val2
                                        }else{null}

                                    ts+","+userid+","+sessionid+","+phpSid+","+notificationid+","+gametype+","+gamevariation+","+chips.toString+","+
                                    coins.toString+","+level.toString+","+xp.toString+","+currentCID+","+experiment+","+promoid+","+chips_reward+","+award_status+","+reward+","+rewardType
                                }
                            case "mpe_ack" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                           compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    //Extract chips
                                    val chips =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                       }else{0.toLong}
                                    //Extract notification id
                                    val notificationid =
                                        if (compact(render(json \ "notificationId")).trim.length > 0 ){
                                            compact(render(json \ "notificationId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract experiment
                                    val experiment =
                                        if (compact(render(json \ "experiment")).trim.length > 0 ){
                                            compact(render(json \ "experiment")).replace("\"", "").trim
                                        }else{null}
                                    //Extract action
                                    val action =
                                        if (compact(render(json \ "action")).trim.length > 0 ){
                                            compact(render(json \ "action")).replace("\"", "").trim
                                        }else{null}
                                    //Extract offer id
                                    val offerId =
                                        if (compact(render(json \ "offerId")).trim.length > 0 ){
                                            compact(render(json \ "offerId")).replace("\"", "").trim
                                        }else{null}
                                    //Rewrite some variables with a different name for MPE NTN
                                    val promotionid = offerId
                                    val promostatus = action
                                    val experimentid = experiment

                                    ts.toString+","+userid+","+sessionid+","+phpSid+","+notificationid+","+chips.toString+","+
                                    coins.toString+","+level.toString+","+xp.toString+","+currentCID+","+experiment+","+action+","+offerId+","+
                                    promotionid+","+promostatus+","+experimentid
                                }

                            case "meta_game_req" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract id
                                    val id =
                                        if (compact(render(json \ "id")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "id")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract previous  Value Rewards
                                    val prevVal_Rewards =
                                        if (compact(render((json \"rewards")(0) \"prevValue")).replace("\"", "").trim.length > 0 ){
                                            compact(render((json \"rewards")(0) \"prevValue")).replace("\"", "").trim
                                        }else{null}
                                    //Extract current Value Rewards
                                    val curVal_Rewards =
                                        if (compact(render((json \"rewards")(0) \"curValue")).replace("\"", "").trim.length > 0 ){
                                            compact(render((json \"rewards")(0) \"curValue")).replace("\"", "").trim
                                        }else{null}
                                    //Extract reward Amount Rewards
                                    val rewardAmount_Rewards =
                                        if (compact(render((json \"rewards")(0) \"rewardAmount")).replace("\"", "").trim.length > 0 ){
                                            compact(render((json \"rewards")(0) \"rewardAmount")).replace("\"", "").trim
                                        }else{null}
                                    //Extract reward Type Rewards
                                    val rewardType_Rewards =
                                        if (compact(render((json \"rewards")(0) \"rewardType")).replace("\"", "").trim.length > 0 ){
                                            compact(render((json \"rewards")(0) \"rewardType")).replace("\"", "").trim
                                        }else{null}
                                    //Extract previous  Value Rewards Pending
                                    val prevVal_RewardsPending =
                                        if (compact(render((json \"rewardsPending")(0) \"prevValue")).replace("\"", "").trim.length > 0 ){
                                            compact(render((json \"rewardsPending")(0) \"prevValue")).replace("\"", "").trim
                                        }else{null}
                                    //Extract current Value Rewards Pending
                                    val curVal_RewardsPending =
                                        if (compact(render((json \"rewardsPending")(0) \"curValue")).replace("\"", "").trim.length > 0 ){
                                            compact(render((json \"rewardsPending")(0) \"curValue")).replace("\"", "").trim
                                        }else{null}
                                    //Extract reward Amount Rewards Pending
                                    val rewardAmount_RewardsPending =
                                        if (compact(render((json \"rewardsPending")(0) \"rewardAmount")).replace("\"", "").trim.length > 0 ){
                                            compact(render((json \"rewardsPending")(0) \"rewardAmount")).replace("\"", "").trim
                                        }else{null}
                                    //Extract reward Type Rewards Pending
                                    val rewardType_RewardsPending =
                                        if (compact(render((json \"rewardsPending")(0) \"rewardType")).replace("\"", "").trim.length > 0 ){
                                            compact(render((json \"rewardsPending")(0) \"rewardType")).replace("\"", "").trim
                                        }else{null}
                                    id+","+sessionid+","+userid+",primaryAward,"+prevVal_Rewards+","+curVal_Rewards+","+rewardAmount_Rewards+","+
                                    rewardType_Rewards+","+ts.toString+"\t"+id+","+sessionid+","+userid+",awardOnAcceptance,"+prevVal_RewardsPending+","+
                                    curVal_RewardsPending+","+rewardAmount_RewardsPending+","+rewardType_RewardsPending+","+ts.toString
                                }
                            case "meta_game_res" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract id
                                    val id =
                                        if (compact(render(json \ "id")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "id")).replace("\"", "").trim
                                        }else{null}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    val awardStatus = "accepted"
                                    //Extract php session id
                                    val phpSid =
                                        if (compact(render(json \ "php_sid")).trim.length > 0 ){
                                            compact(render(json \ "php_sid")).replace("\"", "").trim
                                        }else{null}
                                    //Extract cid
                                    val currentCID  =
                                        if (compact(render(json \ "cur_cid")).trim.length > 0  ){
                                           compact(render(json \ "cur_cid")).replace("\"", "").trim
                                        }else{"0"}
                                    //Extract chips
                                    val chips =
                                        if (compact(render(json \ "chips")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "chips")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract level
                                    val level =
                                        if (compact(render(json \ "level")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "level")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract coins
                                    val coins =
                                        if (compact(render(json \ "coins")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "coins")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract experience
                                    val xp =
                                        if (compact(render(json \ "xp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "xp")).replace("\"", "").trim.toLong
                                       }else{0.toLong}
                                    //Extract session id
                                    val sessionid =
                                        if (compact(render(json \ "sessionId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "sessionId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game
                                    val game =
                                        if (compact(render(json \ "g")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "g")).replace("\"", "").trim
                                        }else{null}
                                    //Extract gs
                                    val gs =
                                        if (compact(render(json \ "gs")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gs")).replace("\"", "").trim
                                        }else{null}
                                    //Extract game variation
                                    val gv =
                                        if (compact(render(json \ "gv")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "gv")).replace("\"", "").trim
                                        }else{null}
                                    //Extract reward amount
                                    val rewardAmount =
                                        if (compact(render(json \ "rewardAmount")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "rewardAmount")).replace("\"", "").trim.toLong
                                       }else{0.toLong}
                                    //Extract reward type
                                    val rewardType =
                                        if (compact(render(json \ "rewardType")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "rewardType")).replace("\"", "").trim
                                        }else{null}
                                    //Extract number of rewards
                                    val numRewards =
                                        if (compact(render(json \ "numRewards")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "numRewards")).replace("\"", "").trim.toLong
                                       }else{0.toLong}
                                    //Extract current value
                                    val curValue =
                                        if (compact(render(json \ "curValue")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "curValue")).replace("\"", "").trim.toLong
                                       }else{0.toLong}
                                    //Extract previous value
                                    val prevValue =
                                        if (compact(render(json \ "prevValue")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "prevValue")).replace("\"", "").trim.toLong
                                       }else{0.toLong}
                                    //Extract requires user confirmation
                                    val requiresUserConfirmation =
                                        if (compact(render(json \ "requiresUserConfirmation")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "requiresUserConfirmation")).replace("\"", "").trim
                                        }else{null}
                                    userid +","+ id +","+ awardStatus +","+ ts.toString +","+ phpSid +","+ currentCID +","+ chips.toString +","+
                                    level.toString +","+  coins.toString +","+  xp.toString +","+  sessionid +","+ game +","+ gs +","+ gv +","+
                                    rewardAmount.toString +","+ rewardType +","+ numRewards.toString +","+ curValue.toString +","+ prevValue.toString +","+ requiresUserConfirmation
                                }
                            case "ab_testing" =>
                                {
                                    //Extract timestamp
                                    val ts =
                                        if (compact(render(json \ "timestamp")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "timestamp")).replace("\"", "").trim.toLong
                                        }else{0.toLong}
                                    //Extract userid
                                    val userid =
                                        if (compact(render(json \ "userId")).replace("\"", "").trim.length > 0 ){
                                            compact(render(json \ "userId")).replace("\"", "").trim
                                        }else{null}
                                    //Extract all the experiments and their values
                                    val result: Map[String, String] = (json \"xptInfo").mapField(
                                        k => {
                                            val v: String =
                                                k._2 match {
                                                    case s: JString => k._2.extract[String]
                                                    case _ => write(k._2)
                                                    }
                                                (k._1, JString(v))
                                        }).extract[Map[String, String]]

                                    val prefixRec = ts.toString+","+userid+","
                                    var finalRec = ""
                                    //Prepare all experiments for printing as output of map function
                                    for ((k,v) <- result) {
                                        finalRec+=prefixRec+k.toString+","+v.toString+"\t"
                                    }
                                    finalRec.trim
                                }
                            case default =>
                                {
                                    compact(render(json)).toString
                                }
                            }
                            (tbl_name,this_records)
            })

            hadoopConf.set("mapreduce.output.basename", System.currentTimeMillis.toString)
            //transformedRDD.saveAsNewAPIHadoopFile("s3n://ddi-business-intelligence/emr_spark_testing_prod/sample/", classOf[String], classOf[String], classOf[TextOutputFormat[String, String]], hadoopConf)

            val sqlContext = new org.apache.spark.sql.SQLContext(sc)
            // this is used to implicitly convert an RDD to a DataFrame.
            import sqlContext.implicits._
            val thisRDD = transformedRDD.toDF("table_name": String, "rowValue": String)
            val modifiedRDD = thisRDD.explode("rowValue", "r1") {mystr:String => mystr.trim.split("\t").toList }
            modifiedRDD.registerTempTable("current_batch")
            sqlContext.sql("select table_name, r1, date_format (current_date(),'yyyy') as year, date_format (current_date(),'MM') as month, date_format (current_date(),'dd') as day, date_format (current_timestamp(),'HH') as hour from current_batch").write.partitionBy("table_name","year","month","day","hour").mode("append").text(s"$processedS3URL")

            val rdd_count = transformedRDD.count()
            sendValue(cloudWatchMetricName, rdd_count, System.currentTimeMillis, dims, appName, awsCloudWatchClient)
            sendValue(cloudWatchMetricName, rdd_count, System.currentTimeMillis, dims1, appName, awsCloudWatchClient)
        }}
    }
    def startStreaming(ssc:org.apache.spark.streaming.StreamingContext)={
        ssc.start()
    }
    def stopStreaming(ssc:org.apache.spark.streaming.StreamingContext) ={
        ssc.stop(stopSparkContext=false, stopGracefully=true)
    }
}

DDI_Stream_Processing.main(Array())
DDI_Stream_Processing.startStreaming(DDI_Stream_Processing.ssc)

DDI_Stream_Processing.stopStreaming(DDI_Stream_Processing.ssc)
